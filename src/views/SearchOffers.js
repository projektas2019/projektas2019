import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { getCategories, getCities } from '../Utils';

class SearchOffers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: "",
      createdFrom: " ",
      createdTo: " ",
      category: "--Pasirinkite kategoriją--",
      city: "--Pasirinkite miestą--",
      rating: "---------------",
      categories: [],
      cities: []
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  async handleClick() {
    let url = "";

    if (this.state.createdFrom === " ") {
      await this.setState({ createdFrom: "" });
    }
    if (this.state.createdTo === " ") {
      await this.setState({ createdTo: "" });
    }
    if (this.state.category === "--Pasirinkite kategoriją--") {
      await this.setState({ category: "" });
    }
    if (this.state.city === "--Pasirinkite miestą--") {
      await this.setState({ city: "" });
    }
    if (this.state.rating === "---------------") {
      await this.setState({ rating: "" });
    }

    if (this.state.query.length > 0) {
      url = url + 'query=' + this.state.query;
    }
    if (this.state.createdFrom.length > 0) {
      if (url.length > 0) {
        url = url + '&';
      }
      url = url + 'createdFrom=' + this.state.createdFrom;
    }
    if (this.state.createdTo.length > 0) {
      if (url.length > 0) {
        url = url + '&';
      }
      url = url + 'createdTo=' + this.state.createdTo;
    }
    if (this.state.category.length > 0) {
      if (url.length > 0) {
        url = url + '&';
      }
      url = url + 'category=' + this.state.category;
    }
    if (this.state.city.length > 0) {
      if (url.length > 0) {
        url = url + '&';
      }
      url = url + 'city=' + this.state.city;
    }
    if (this.state.rating.length > 0) {
      if (url.length > 0) {
        url = url + '&';
      }
      url = url + 'rating=' + this.state.rating;
    }

    this.props.history.push('/search/offers/results/?' + url)
  }

  async componentDidMount() {
    await getCategories()
      .then((result) => {
        this.setState({
          categories: result
        })
      })
    await getCities()
      .then((result) => {
        this.setState({
          cities: result
        })
      })
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  render() {
    let ratings = [];
    for (let i = 5; i >= 0.1; i -= 0.1) {
      i = Math.round(i * 100) / 100;
      ratings.push(<option key={i} value={i}>{i}</option>);
    }

    return (
      <main>
        <Typography variant="h4" style={{ paddingTop: "20px", display: "inline-block" }}>Įveskite paiešką:</Typography>
        <TextField
          margin="normal"
          variant="outlined"
          name="query"
          type="text"
          style={{ paddingLeft: "20px", width: "800px" }}
          value={this.state.query}
          onChange={this.handleChange}
        />
        <Button variant="contained" color="primary" style={{ height: "50px", width: "100px", marginLeft: "30px", position: "relative", bottom: "5px" }} onClick={this.handleClick}>
          Ieškoti
        </Button>
        <Typography variant="h5" style={{ paddingTop: "20px" }}>Kiti pasirinkimai:</Typography>
        <br />
        <TextField
          label="Nuo"
          margin="normal"
          variant="outlined"
          name="createdFrom"
          type="date"
          style={{ display: "inline-block" }}
          value={this.state.createdFrom}
          onChange={this.handleChange}
        />
        <span style={{ fontSize: "20px", position: "relative", top: "32px" }}> - </span>
        <TextField
          label="Iki"
          margin="normal"
          variant="outlined"
          name="createdTo"
          type="date"
          style={{ display: "inline-block" }}
          value={this.state.createdTo}
          onChange={this.handleChange}
        />
        <br />
        <TextField
          select
          label="Kategorija"
          name="category"
          value={this.state.category}
          onChange={this.handleChange}
          style={{width: "320px"}}
          SelectProps={{
            native: true
          }}
          margin="normal"
          variant="outlined"
        >
          <option>--Pasirinkite kategoriją--</option>
          {this.state.categories.map(category =>
            <option key={category} value={category}>{category}</option>)}
        </TextField>
        <br />
        <TextField
          select
          label="Miestas"
          name="city"
          value={this.state.city}
          onChange={this.handleChange}
          style={{width: "320px"}}
          SelectProps={{
            native: true
          }}
          margin="normal"
          variant="outlined"
        >
          <option>--Pasirinkite miestą--</option>
          {this.state.cities.map(city =>
            <option key={city} value={city}>{city}</option>)}
        </TextField>
        <br />
        <TextField
          select
          label="Minimalus įvertinimas"
          name="rating"
          value={this.state.rating}
          onChange={this.handleChange}
          style={{width: "320px"}}
          SelectProps={{
            native: true
          }}
          margin="normal"
          variant="outlined"
        >
          <option>---------------</option>
          {ratings}
        </TextField>
      </main>
    );
  }
}

export default SearchOffers;