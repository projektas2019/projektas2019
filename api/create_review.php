<?php
  
  include_once 'config.php';
  include_once 'classes\User.php';
  include_once 'classes\Review.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['username'];
  $offer = $escaped['offer'];
  $title = $escaped['reviewTitle'];
  $review = $escaped['review'];
  $rating = $escaped['reviewRating'];

  $usersObj = new User();
  $user = $usersObj->getUser($username);
  
  $reviewsObj = new Review();
  $message = $reviewsObj->createReview($user['id'], $offer, $title, $review, $rating);

  echo json_encode($message);

?>

