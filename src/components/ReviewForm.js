import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import ErrorIcon from '@material-ui/icons/Error';
import './ReviewForm.css';

function ReviewForm(props) {
  return (
    <div>
      <Typography variant="h6" style={{ padding: "20px 0 10px" }}>{props.editing ? "" : "Naujas atsiliepimas"}</Typography>
      <form>
        <TextField
          label="Pavadinimas"
          margin="normal"
          variant="outlined"
          name="reviewTitle"
          inputProps={{ maxLength: 64 }}
          style={{ width: "800px" }}
          type="text"
          value={props.data.reviewTitle}
          onChange={props.handleChange}
        />
        <br />
        <TextField
          variant="outlined"
          name="review"
          multiline
          rows="6"
          inputProps={{ maxLength: 500 }}
          style={{ width: "800px" }}
          type="text"
          value={props.data.review}
          onChange={props.handleChange}
        />
        <br />
        <Typography style={{ fontSize: "20px", paddingTop: "10px" }}>Įvertinimas: </Typography>
        <StarRatingComponent
          name="rating"
          starCount={5}
          value={props.data.reviewRating}
          onStarClick={props.onStarClick}
        />
        <Button variant="contained" color="primary" style={{ margin: "10px 0", display: "block" }} onClick={props.createReview}>{props.editing ? "Išsaugoti" : "Pridėti atsiliepimą"}</Button>
        {props.data.reviewError !== "" ?
          <Paper style={{ padding: "10px", backgroundColor: "#F44336", boxShadow: "none", margin: "10px 0", display: "inline-block" }}>
            <ErrorIcon style={{ color: "#FAFAFA", position: "relative", top: "3px" }} /><Typography style={{ color: "#FAFAFA", fontWeight: "bold", display: "inline", paddingLeft: "10px", position: "relative", bottom: "4px" }}>{props.data.reviewError}</Typography>
          </Paper> :
          <Typography></Typography>}
      </form>
    </div>
  )
}

export default ReviewForm;