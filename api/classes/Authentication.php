<?php

include_once 'Database.php';
include_once 'User.php';
include_once 'libs/php-jwt/BeforeValidException.php';
include_once 'libs/php-jwt/ExpiredException.php';
include_once 'libs/php-jwt/SignatureInvalidException.php';
include_once 'libs/php-jwt/JWT.php';
use \Firebase\JWT\JWT;

class Authentication {

  private static $key = "example";
  private static $iss = "http://example.org";
  private static $aud = "http://example.com";
  private static $iat = 1356999524;
  private static $nbf = 1357000000;

  public static function login($username, $password){
    if($username == ""){
      return 'Įveskite vartotojo vardą';
    }
    if($password == ""){
      return 'Įveskite slaptažodį';
    }

    $usersObj = new User();
    $user = $usersObj->getUser($username);

    if(!$user){
      return 'Vartotojas neegzistuoja';
    }
    if($user['role'] === 'banned'){
      return 'Vartotojas užblokuotas';
    }
    if(!password_verify($password, $user['password'])){
      return 'Slaptažodis neteisingas';
    }

    return Authentication::createToken($user);
  }

  public static function register($username, $email, $password, $confirmPassword){
    
    if($username == ""){
      return 'Įveskite vartotojo vardą';
    }
    if($email === ""){
      return 'Įveskite elektroninį paštą';
    }
    if($password == ""){
      return 'Įveskite slaptažodį';
    }
    if(!(strlen($password) > 8 && preg_match('~[0-9]~', $password) && preg_match('~[A-Z]~', $password))){
      return 'Slaptažodis turi būti bent 8 simbolių ilgio, turėti skaičių ir didžiąją raidę.';
    }
    if($confirmPassword == ""){
      return 'Pakartokite slaptažodį';
    }
    if($password != $confirmPassword){
      return 'Slaptažodžiai nesutampa';
    }
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      return 'Elektroninis paštas netinkamas';
    }
    $usersObj = new User();
    $userExists = $usersObj->getUserByEmail($email);
    if($userExists){
      return 'Elektroninis paštas jau egzistuoja';
    }
    $user = $usersObj->getUser($username);
    if($user){
      return 'Vartotojas jau egzistuoja';
    }

    $usersObj->createUser($username, $password, $email);
    return 'Vartotojas užregistruotas';
  }

  public static function createToken($user){
    $token = array(
      "iss" => Authentication::$iss,
      "aud" => Authentication::$aud,
      "iat" => Authentication::$iat,
      "nbf" => Authentication::$nbf,
      "data" => array(
        "id" => $user['id'],
        "username" => $user['username'],
        "email" => $user['email'],
        "role" => $user['role']
      )
    );

    return JWT::encode($token, Authentication::$key);
  }

  public static function validateToken($jwt){
    if(!$jwt){
      return "Access denied";
    }

    try {
      $decoded = JWT::decode($jwt, Authentication::$key, array('HS256'));
      return $decoded->data;
    }
    catch (Exception $e) {
      return $e->getMessage();
    }
  }
}


?>