<?php

include_once 'Database.php';
include_once 'Category.php';
include_once 'City.php';

class Offer {

  private $offers_table = '';
  private $users_table = '';
  private $cities_table = '';
  private $categories_table = '';
  private $reviews_table = '';

  public function __construct() {
    $this->offers_table = 'offers';
    $this->users_table = 'users';
    $this->cities_table = 'cities';
    $this->categories_table = 'categories';
    $this->reviews_table = 'reviews';
  }

  public function createOffer($user, $title, $description, $category, $location, $phone){
    if($title === ""){
      return "Įveskite pasiūlymo pavadinimą";
    }
    if($description === ""){
      return "Įveskite pasiūlymo aprašymą";
    }
    if($phone === ""){
      return "Įveskite savo telefono numerį";
    }
    if(!(preg_match('/^[+370][0-9]*$/', $phone) && strlen($phone) === 12) &&
       !(preg_match('/^[8][0-9]*$/', $phone) && strlen($phone) === 9)){
      return "Telefono numerio formatas turi būti +370******** arba 8********";
    }
  
    date_default_timezone_set('Europe/Riga');
    $creation_date = date('Y-m-d H:i:s');
  
    $query = "INSERT INTO `{$this->offers_table}` 
              (
                title, 
                creation_date, 
                category, 
                description, 
                location, 
                phone, 
                status, 
                fk_user
              ) 
              VALUES 
              (
                '$title', 
                '$creation_date', 
                '$category', 
                '$description', 
                '$location', 
                '$phone', 
                'open', 
                '$user'
              )";
              
    if(Database::query($query)){
      return 'Offer created';
    } else {
      return 'Įvyko klaida kuriant pasiūlymą';
    }
  }

  public function getOffer($id){
    $query = "SELECT 
                    `{$this->offers_table}`.`id`, 
                    `{$this->offers_table}`.`title`,
                    `{$this->offers_table}`.`creation_date` AS `created`,
                    `{$this->offers_table}`.`category`,
                    `{$this->offers_table}`.`description`,
                    `{$this->offers_table}`.`location`,
                    `{$this->offers_table}`.`phone`,
                    `{$this->offers_table}`.`status`,
                    `{$this->users_table}`.`username` AS `user`,
                    `{$this->categories_table}`.`category` AS `category`,
                    `{$this->cities_table}`.`name` AS `location`,
                    IFNULL(AVG(`{$this->reviews_table}`.`rating`),0) AS `rating`
              FROM `{$this->offers_table}`
              INNER JOIN `{$this->users_table}` ON `{$this->offers_table}`.`fk_user`=`{$this->users_table}`.`id`
              INNER JOIN `{$this->categories_table}` ON `{$this->offers_table}`.`category`=`{$this->categories_table}`.`id`
              INNER JOIN `{$this->cities_table}` ON `{$this->offers_table}`.`location`=`{$this->cities_table}`.`id`
              LEFT JOIN `{$this->reviews_table}` ON `{$this->offers_table}`.`id`=`{$this->reviews_table}`.`fk_offer`
              WHERE `{$this->offers_table}`.`id`='$id'
              GROUP BY `{$this->offers_table}`.`id`";

    $data = Database::select($query);

    if(!$data){
      return false;
    }

    return $data[0];
  }

  public function listOffers(){
    $query = "SELECT 
                    `{$this->offers_table}`.`id`, 
                    `{$this->offers_table}`.`title`,
                    `{$this->offers_table}`.`creation_date` AS `created`,
                    `{$this->offers_table}`.`category`,
                    `{$this->offers_table}`.`description`,
                    `{$this->offers_table}`.`location`,
                    `{$this->offers_table}`.`phone`,
                    `{$this->offers_table}`.`status`,
                    `{$this->users_table}`.`username` AS `user`,
                    `{$this->categories_table}`.`category` AS `category`,
                    `{$this->cities_table}`.`name` AS `location`,
                    IFNULL(AVG(`{$this->reviews_table}`.`rating`),0) AS `rating`
              FROM `{$this->offers_table}`
              INNER JOIN `{$this->users_table}` ON `{$this->offers_table}`.`fk_user`=`{$this->users_table}`.`id`
              INNER JOIN `{$this->categories_table}` ON `{$this->offers_table}`.`category`=`{$this->categories_table}`.`id`
              INNER JOIN `{$this->cities_table}` ON `{$this->offers_table}`.`location`=`{$this->cities_table}`.`id`
              LEFT JOIN `{$this->reviews_table}` ON `{$this->offers_table}`.`id`=`{$this->reviews_table}`.`fk_offer`
              GROUP BY `{$this->offers_table}`.`id`
              ORDER BY `{$this->offers_table}`.`creation_date` DESC";

    $data = Database::select($query);

    return $data;
  }

  public function updateOffer($id, $title, $description, $category, $location, $phone){
    if($title === ""){
      return "Įveskite pasiūlymo pavadinimą";
    }
    if($description === ""){
      return "Įveskite pasiūlymo aprašymą";
    }
    if($category === "" || $category === "--Pasirinkite kategoriją--"){
      return "Pasirinkite kategoriją";
    }
    if($location === "" || $location === "--Pasirinkite miestą--"){
      return "Pasirinkite miestą";
    }
    if($phone === ""){
      return "Įveskite savo telefono numerį";
    }
    if(!(preg_match('/^[+370][0-9]*$/', $phone) && strlen($phone) === 12) &&
       !(preg_match('/^[8][0-9]*$/', $phone) && strlen($phone) === 9)){
        return "Telefono numerio formatas turi būti +370******** arba 8********";
    }

    $categoriesObj = new Category();
    $data = $categoriesObj->getCategory($category);
    $category = $data['id'];

    $citiesObj = new City();
    $data = $citiesObj->getCity($location);
    $location = $data['id'];
    
    $query = "UPDATE `{$this->offers_table}` 
              SET 
                  `title`='$title', 
                  `description`='$description', 
                  `category`='$category', 
                  `location`='$location', 
                  `phone`='$phone' 
              WHERE `id`='$id'";

    Database::query($query);

    return 'Offer updated';
  }

  public function deleteOffer($id){
    $query = "DELETE FROM `{$this->reviews_table}` 
              WHERE fk_offer='$id'";
    Database::query($query);

    $query = "DELETE FROM `{$this->offers_table}` 
              WHERE id='$id'";
    Database::query($query);
  }

  public function updateRating($id){
    $query = "SELECT AVG(rating) AS rating 
              FROM `{$this->reviews_table}` 
              WHERE fk_offer='$id'";

    $result = Database::select($query);
    
    if(!$result){
      return false;
    }

    return $result[0]['rating'];
  }
}


?>