<?php

use PHPUnit\Framework\TestCase;
require_once 'api/classes/Category.php';
require_once 'api/classes/Database.php';

class CategoryTest extends TestCase {

  private $db_link;
  private $mysqli;  
  private $categorysObj;

  protected function setUp() :void {
    $this->mysqli = new mysqli("localhost", "root", "", "testdb");
    $sql = file_get_contents('tests/unit/testdb.sql');

    $queries = preg_split('/(;)/', $sql, -1, PREG_SPLIT_DELIM_CAPTURE);
    $queriesWithDelimiters = array();
    for ($i = 0, $n = count($queries) - 1; $i < $n; $i += 2) {
      $queriesWithDelimiters[] = $queries[$i].$queries[$i + 1];
    }
    if ($queries[$n] != '') {
      $queriesWithDelimiters[] = $queries[$n];
    }
    
    foreach ($queriesWithDelimiters as $query) {
      $this->mysqli->query($query) or trigger_error($this->mysqli->error);
    }

    Database::$db = 'testdb';
    $this->categorysObj = new Category();
  } 

  public function testGetCategorySuccess() {
    $message = $this->categorysObj->getCategory(1);

    $this->assertEquals($message, false);
  }
  public function testGetCategoryFailure() {
    $message = $this->categorysObj->getCategory(1000);

    $this->assertEquals($message, false);
  }
  public function testListCategoriesSuccess() {
    $expected = [
      [
        "id" => 8,
        "category" => "Å aldytuvai"
      ],
      [
        "id" => 9,
        "category" => "Baldai"
      ],
      [
        "id" => 4,
        "category" => "Elektronika"
      ],
      [
        "id" => 1,
        "category" => "Kompiuteriai"
      ],
      [
        "id" => 5,
        "category" => "MikrobangÅ³ krosnelÄ—s"
      ],
      [
        "id" => 6,
        "category" => "Monitoriai"
      ],
      [
        "id" => 10,
        "category" => "PrograminÄ— Ä¯ranga"
      ],
      [
        "id" => 2,
        "category" => "Telefonai"
      ],
      [
        "id" => 3,
        "category" => "Televizoriai"
      ],
      [
        "id" => 7,
        "category" => "Virduliai"
      ]
    ];
    $data = $this->categorysObj->listCategories();

    $this->assertEquals($data, $expected);
  }





  protected function tearDown() :void {
    $this->db_link = mysqli_connect('localhost', 'root', '');
    $sql = 'DROP DATABASE testdb';
    mysqli_query($this->db_link, $sql);
    $sql = 'CREATE DATABASE testdb';
    mysqli_query($this->db_link, $sql);
  }
}

?>