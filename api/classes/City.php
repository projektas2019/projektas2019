<?php

include_once 'Database.php';

class City {

  private $cities_table = '';

  public function __construct() {
    $this->cities_table = 'cities';
  }

  public function getCity($city){
    if($city === "" || $city === "--Pasirinkite miestą--"){
      return NULL;
    }

    $query = "SELECT 
                     `{$this->cities_table}`.`id`
              FROM  `{$this->cities_table}`
              WHERE `{$this->cities_table}`.`name`='$city'";

    $data = Database::select($query);

    if(!$data){
      return false;
    }
    
    return $data[0];
  }

  public function listCities(){
    $query = "SELECT 
                      id,
                      name
              FROM `{$this->cities_table}` 
              ORDER BY name";
    $data = Database::select($query);

    return $data;
  }
}

?>