// Bendra POST fetch funkcija
export const fetchPost = (filename, data) => {
  let url = 'http://localhost/repair-website/api/';

  return new Promise((resolve, reject) => {
    fetch(url + filename, {
      method: 'POST',
      header: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then((response) => response.json())
    .then((responseJson) => {
      resolve(responseJson);
    })
    .catch((error) => {
      reject(error);
    });
  });
}

// Funkcija, gaunanti visas kategorijas
export const getCategories = () => {
  return fetchPost('get_categories.php', {});
}

// Funkcija, gaunanti visus miestus
export const getCities = () => {
  return fetchPost('get_cities.php', {});
}
