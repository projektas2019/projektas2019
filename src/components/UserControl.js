import React from 'react';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { Typography } from '@material-ui/core';

const styles = ({
  logoutButton: {
    backgroundColor: "#B71C1C",
    color: "#FAFAFA",
    fontWeight: "bold",
    margin: "5px"
  },
  toolbar: {
    backgroundColor: "#f3e8e8",
    minHeight: "0",
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingRight: "0"
  }
});

function UserControl(props) {

  function handleLogout() {
    const cookies = new Cookies();
    cookies.remove('jwt', { path: '/' });
    props.childProps.userHasLoggedIn(false);
    props.childProps.history.push('/');
  }

  return (
    <AppBar position="static">
      <Toolbar style={styles.toolbar}>
        {props.childProps.userRole === 'unverified' ?
          <Typography style={{paddingRight: "30px", color: "red", fontWeight: "bold"}}>Patvirtinkite savo elektroninį paštą!</Typography> :
          <Typography></Typography>}
        <Typography variant="body2" style={{paddingRight: "20px"}}>
          Prisijungęs vartotojas: <Link style={{color: "black", fontWeight: "bold"}} to={"/accounts/" + props.childProps.currentUser} className="link">{props.childProps.currentUser}</Link>
        </Typography>
        <Button variant="contained" style={styles.logoutButton} onClick={handleLogout}>Atsijungti</Button>
      </Toolbar>
    </AppBar>
  );
}

export default UserControl;