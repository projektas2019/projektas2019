import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const styles = {
  container: {
    margin: "20px 40px 0 0",
    border: "1px solid #616161"
  },
  link: {
    display: "block",
    borderTop: "1px solid #616161",
    padding: "10px"
  }
}


function AccountSettings(props) {
  return (
    <div>
      <div style={styles.container}>
        <Typography variant="h5" style={{padding: "10px"}}>Paskyros valdymas</Typography>
        <Link href={props.data.username + "/change-password"} style={styles.link} >Pakeisti slaptažodį</Link>
        <Link href={props.data.username + "/update-account"} style={styles.link} >Atnaujinti informaciją</Link>
      </div>
    </div>
  );
}

export default AccountSettings;