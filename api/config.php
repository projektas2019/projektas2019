<?php
  header("Access-Control-Allow-Origin: *");
  
  $servername = "localhost";
  $db_username = "root";
  $db_password = "";
  $dbname = "repair_db";

  $conn = new mysqli($servername, $db_username, $db_password, $dbname);
  mysqli_query($conn, "SET NAMES 'utf8'");
  
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

?>