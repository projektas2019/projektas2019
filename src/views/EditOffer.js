import React, { Component } from 'react';
import CreateOffer from './CreateOffer';
import { fetchPost } from '../Utils';

class EditOffer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      location: "",
      category: "",
      phone: "",
      user: "",
      isLoading: true
    };
    this.getOfferData = this.getOfferData.bind(this);
  }

  async componentDidMount() {
    await this.getOfferData();
    this.setState({ isLoading: false });
  }

  async getOfferData() {
    await fetchPost('get_offer.php', { id: this.props.match.params.id })
      .then((result) => {
        this.setState({
          title: result.title,
          description: result.description,
          category: result.category,
          location: result.location,
          phone: result.phone,
          user: result.user
        });
      })
  }

  selectElement() {
    if (this.state.isLoading) {
      return <main></main>
    }

    return <CreateOffer
      childProps={this.props}
      edit={true}
      data={this.state}
      id={this.props.match.params.id}
    />
  }

  render() {
    return (
      <div>
        {this.selectElement()}
      </div>
    );
  }
}

export default EditOffer;