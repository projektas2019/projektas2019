<?php
  define ('SITE_ROOT', dirname(__DIR__, 1));

  include_once 'config.php';

  if(isset($_FILES['images'])){
    foreach($_FILES["images"]["tmp_name"] as $key=>$tmp_name) {
      $fileTmpLoc = $_FILES["images"]["tmp_name"][$key];
      $fileName = $_POST["imageNames"][$key];
      $fileSize = $_FILES["images"]["size"][$key];

      if(!$fileTmpLoc){
        echo "Error";
        exit();
      }

      if(move_uploaded_file($fileTmpLoc, SITE_ROOT."/public/images/uploads/$fileName")){
        echo "$fileName uploaded" . SITE_ROOT."/public/images/uploads/$fileName";
      } else {
        echo (SITE_ROOT."/uploads/$fileName");
      }
    }
  }
?>