<?php

  header("Access-Control-Allow-Origin: *");

  include 'classes\Authentication.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json);
  $jwt = isset($obj->jwt) ? $obj->jwt : "";

  $message = Authentication::validateToken($jwt);

  echo json_encode($message);

?>