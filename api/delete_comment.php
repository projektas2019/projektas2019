<?php
  
  include_once 'config.php';
  include_once 'classes\Comment.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];

  $commentsObj = new Comment();
  $commentsObj->deleteComment($id);

  echo json_encode('Deleted comment');

?>