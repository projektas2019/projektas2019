<?php

  define ('SITE_ROOT', dirname(__DIR__, 1));
  include_once 'config.php';
  include_once 'classes\Post.php';
  include_once 'classes\Image.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];
  $title = $escaped['title'];
  $description = $escaped['description'];
  $category = $escaped['category'];
  $location = $escaped['location'];
  $phone = $escaped['phone'];
  $price = $escaped['price'];
  $imageNames = $escaped['imageNames'];

  $postsObj = new Post();
  $message = $postsObj->updatePost($id, $title, $description, $category, $location, $phone, $price);

  if($message === 'Post updated'){
    $imagesObj = new Image();
    $imagesData = $imagesObj->getImages($id);
    $images = array();
    for($i = 0; $i < count($imagesData); $i++){
      $images[] = $imagesData[$i]['image'];
    }
  
    if($imageNames !== $images){
      $imagesObj->deleteImages($id, SITE_ROOT);
      $postsObj->insertImages($id, $imageNames);
    }
  }
  
  echo json_encode($message);

?>