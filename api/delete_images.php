<?php

  header("Access-Control-Allow-Origin: *");
  define ('SITE_ROOT', dirname(__DIR__, 1));

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $imageNames = $obj['imageNames'];

  for($i = 0; $i < count($imageNames); $i++){
    unlink(SITE_ROOT . "/uploads/" . $imageNames[$i]);
  }

?>