import React from 'react';
import Typography from '@material-ui/core/Typography';

function Error() {
  return (
    <div>
      <main>
        <Typography variant="h5" style={{paddingTop: "20px"}} >Šis puslapis neegzistuoja.</Typography>
      </main>
    </div>
  );
}

export default Error;