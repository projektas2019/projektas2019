import React, { Component } from 'react';
import MiniOffer from '../components/MiniOffer';
import MiniPost from '../components/MiniPost';
import { fetchPost } from '../Utils.js';
import Typography from '@material-ui/core/Typography';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      offers: [],
      isLoading: true
    }
    this.selectElement = this.selectElement.bind(this);
  }

  componentDidMount() {
    fetchPost('get_offers.php', {})
      .then((result) => {
        this.setState({
          offers: result,
          isLoading: false
        })
      });
    fetchPost('get_posts.php', {})
      .then((result) => {
        this.setState({
          posts: result,
          isLoading: false
        })
      });
  }


  selectElement() {
    if (this.state.isLoading) {
      return <p></p>
    } else {
      const currentOffers = this.state.offers.slice(0, 5);
      const newestOffers = currentOffers.map(item => <MiniOffer key={item.id} data={item} />);
      const currentPosts = this.state.posts.slice(0, 5);
      const newestPosts = currentPosts.map(item => <MiniPost key={item.id} data={item} />);

      return <div style={{ display: "flex", justifyContent: "space-around", paddingTop: "20px" }}>
        <div style={{ width: "40%" }} >
          <Typography variant="h4" style={{ textAlign: "center", fontWeight: "bold", paddingBottom: "10px" }}>Naujausi pasiūlymai</Typography>
          {newestOffers}
        </div>
        <div style={{ width: "40%" }} >
          <Typography variant="h4" style={{ textAlign: "center", fontWeight: "bold", paddingBottom: "10px" }}>Naujausi skelbimai</Typography>
          {newestPosts}
        </div>
      </div>
    }
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default Home;