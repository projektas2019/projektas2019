<?php
  
  include_once 'config.php';
  include_once 'classes\User.php';
  include_once 'classes\Category.php';
  include_once 'classes\Offer.php';
  include_once 'classes\City.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['user'];
  $title = $escaped['title'];
  $description = $escaped['description'];
  $category = $escaped['category'];
  $location = $escaped['location'];
  $phone = $escaped['phone'];

  $categoriesObj = new Category();
  $categoryId = $categoriesObj->getCategory($category);
  if($categoryId === NULL){
    echo json_encode('Pasirinkite kategoriją');
    return;
  }
  
  $citiesObj = new City();
  $city = $citiesObj->getCity($location);
  if($city === NULL){
    echo json_encode('Pasirinkite miestą');
    return;
  }

  $usersObj = new User();
  $user = $usersObj->getUser($username);

  $offersObj = new Offer();
  $message = $offersObj->createOffer($user['id'], $title, $description, $categoryId['id'], $city['id'], $phone);

  echo json_encode($message);

?>

