<?php
  
  include_once 'config.php';
  include_once 'classes\Review.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];

  $reviewsObj = new Review();
  $data = $reviewsObj->listReviews($id);

  $reviews = array();
  for($i = 0; $i < count($data); $i++) {
    $reviews[] = array(
      'id' => $data[$i]['id'],
      'reviewTitle' => $data[$i]['title'],
      'review' => $data[$i]['description'],
      'reviewRating' => $data[$i]['rating'],
      'created' => $data[$i]['created'],
      'user' => $data[$i]['user']
    );
  }

  echo json_encode($reviews);
?>