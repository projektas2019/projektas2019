import React from 'react';
import Typography from '@material-ui/core/Typography';

function About() {
  return (
    <div>
      <main className="aboutUs">
        <Typography variant="h5" style={{ padding: "20px 0 10px" }} >Apie tinklapį:</Typography>
        <Typography>Taisymo skelbimų portalas – tai sistema, kuri leidžia jums
          kurti skelbimus apie jūsų neveikiantį daiktą ir surasti žmones,
            kurie jums tą daiktą pataisys už sutartinę kainą.</Typography>
        <Typography variant="h5" style={{ padding: "20px 0 10px" }} >Mūsų kontaktai:</Typography>
        <Typography>Lukas Pupelis: lukas.pupelis@ktu.edu</Typography>
        <Typography>Lukas Tamašauskas: lukas.tamasauskas@ktu.edu</Typography>
      </main>
    </div>
  );
}

export default About;