<?php
  
  include_once 'config.php';
  include_once 'classes\Comment.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['commentEditing'];
  $comment = $escaped['comment'];
  $price = $escaped['commentPrice'];

  $commentsObj = new Comment();
  $message = $commentsObj->updateComment($id, $comment, $price);

  echo json_encode($message);

?>