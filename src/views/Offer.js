import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import OfferContainer from '../components/OfferContainer';
import { fetchPost } from '../Utils.js';

class Offer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      created: "",
      category: "",
      description: "",
      location: "",
      phone: "",
      price: 0,
      rating: 0,
      user: "",
      isLoading: true,
      reviews: [],
      review: "",
      reviewTitle: "",
      reviewRating: 0,
      reviewError: "",
      reviewEditing: 0
    }
    this.selectElement = this.selectElement.bind(this);
    this.deleteOffer = this.deleteOffer.bind(this);
    this.createReview = this.createReview.bind(this);
    this.editReview = this.editReview.bind(this);
    this.deleteReview = this.deleteReview.bind(this);
    this.listReviews = this.listReviews.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onStarClick = this.onStarClick.bind(this);
    this.updateRating = this.updateRating.bind(this);
  }

  onStarClick(nextValue) {
    if (this.state.reviewEditing > 0) {
      for (let i = 0; i < this.state.reviews.length; i++) {
        if (this.state.reviews[i].id === this.state.reviewEditing) {
          const newReviews = this.state.reviews.slice();
          newReviews[i].reviewRating = nextValue;
          this.setState({ reviews: newReviews });
        }
      }
    }
    this.setState({ reviewRating: nextValue });
  }

  async createReview(event) {
    event.preventDefault();

    if (this.state.reviewEditing > 0) {
      for (let i = 0; i < this.state.reviews.length; i++) {
        if (this.state.reviews[i].id === this.state.reviewEditing) {
          await this.setState({
            review: this.state.reviews[i].review,
            reviewTitle: this.state.reviews[i].reviewTitle,
            reviewRating: this.state.reviews[i].reviewRating
          });
        }
      }

      fetchPost('update_review.php', this.state)
        .then((result) => {
          if (result !== 'Review updated') {
            this.setState({
              reviewError: result
            })
          } else {
            this.setState({
              reviewError: "",
              review: "",
              reviewTitle: "",
              reviewRating: 0
            })
            this.listReviews()
              .then(() => {
                this.setState({ reviewEditing: 0 })
                this.updateRating();
              })
          }
        })
    } else {
      const jsonArray = Object.assign(this.state, { username: this.props.currentUser, offer: this.props.match.params.id });
      fetchPost('create_review.php', jsonArray)
        .then((result) => {
          if (result !== 'Review created') {
            this.setState({
              reviewError: result
            })
          } else {
            this.setState({
              reviewError: "",
              review: "",
              reviewTitle: "",
              reviewRating: 0
            })
            this.listReviews();
            this.updateRating();
          }
        })
    }
  }

  updateRating() {
    fetchPost('update_rating.php', { id: this.props.match.params.id })
      .then((result) => {
        this.setState({
          rating: Math.round(result * 100) / 100
        })
      })
  }

  deleteReview(reviewId) {
    fetchPost('delete_review.php', { id: reviewId })
      .then(() => {
        this.listReviews();
        this.updateRating();
      })
  }

  async editReview(reviewId) {
    await this.listReviews();

    this.setState({
      reviewEditing: reviewId,
      reviewError: ""
    })
  }

  async listReviews() {
    await fetchPost('list_reviews.php', { id: this.props.match.params.id })
      .then((result) => {
        this.setState({
          reviews: result
        });
      })
  }

  async deleteOffer() {
    await fetchPost('delete_offer.php', { id: this.props.match.params.id });

    this.props.history.push('/offers');
  }

  async componentWillMount() {
    await fetchPost('get_offer.php', { id: this.props.match.params.id })
      .then((result) => {
        this.setState({
          title: result.title,
          created: result.created,
          category: result.category,
          description: result.description,
          location: result.location,
          phone: result.phone,
          user: result.user,
          rating: Math.round(result.rating * 100) / 100
        });
      });

    await this.listReviews();
    this.setState({ isLoading: false });
  }

  handleChange(event) {
    const { name, value } = event.target;
    if (this.state.reviewEditing > 0) {
      for (let i = 0; i < this.state.reviews.length; i++) {
        if (this.state.reviews[i].id === this.state.reviewEditing) {
          const newReviews = this.state.reviews.slice();
          newReviews[i][name] = value;
          this.setState({ reviews: newReviews });
        }
      }
    } else {
      this.setState({
        [name]: value
      });
    }
  };

  selectElement() {
    if (this.state.isLoading) {
      return <p></p>
    }
    if (this.state.title && this.state.title.length !== "") {
      if (this.props.userRole === "admin" && this.props.currentUser !== this.state.user) {
        return <div className="offer">
          <div style={{ display: "flex", justifyContent: "flex-end", float: "right"}}>
            <Button
              variant="contained"
              color="secondary"
              onClick={this.deleteOffer}
              style={{ margin: "10px" }}
            >
              Ištrinti pasiūlymą
              </Button>
          </div>
          <OfferContainer
            data={this.state}
            childProps={this.props}
            createReview={this.createReview}
            editReview={this.editReview}
            deleteReview={this.deleteReview}
            handleChange={this.handleChange}
            onStarClick={this.onStarClick}
          />
        </div>
      }
      return <div className="offer">
        {(this.props.currentUser === this.state.user && this.props.userRole !== "admin") ||
          (this.props.userRole === "admin" && this.props.currentUser) === this.state.user ?
          <div style={{ display: "flex", justifyContent: "flex-end", float: "right"}}>
            <Button
              variant="contained"
              color="primary"
              href={"/offers/edit/" + this.props.match.params.id}
              style={{ margin: "10px" }}
            >
              Redaguoti pasiūlymą
              </Button>
            <Button
              variant="contained"
              color="secondary"
              style={{ margin: "10px" }}
              onClick={this.deleteOffer}
            >
              Ištrinti pasiūlymą
              </Button>
          </div> :
          <p></p>}
        <OfferContainer
          data={this.state}
          childProps={this.props}
          createReview={this.createReview}
          editReview={this.editReview}
          deleteReview={this.deleteReview}
          handleChange={this.handleChange}
          onStarClick={this.onStarClick}
        />
      </div>
    }
    return <Typography variant="h5" style={{ paddingTop: "20px" }}>Pasiūlymas neegzistuoja.</Typography>
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default Offer;