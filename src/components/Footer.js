import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    width: "100%",
    backgroundColor: "#616161",
    padding: "10px",
    height: "40px",
    margin: "0",
    justifyContent: "left",
    position: "relative",
    clear: "both"
  },
  footerText: {
    color: "#FAFAFA"
  }
};

function Footer() {
  return (
    <div style={styles.root}>
      <Typography style={styles.footerText} variant="body2">Tinklalapį sukūrė LLME</Typography>
    </div>
  );
}

export default withStyles(styles)(Footer);