<?php

include_once 'Database.php';
include_once 'Image.php';

class Search {

  private $offers_table = '';
  private $posts_table = '';
  private $users_table = '';
  private $categories_table = '';
  private $cities_table = '';
  private $reviews_table = '';
  private $images_table = '';

  public function __construct() {
    $this->offers_table = 'offers';
    $this->posts_table = 'posts';
    $this->users_table = 'users';
    $this->categories_table = 'categories';
    $this->cities_table = 'cities';
    $this->reviews_table = 'reviews';
    $this->images_table = 'images';
  }

  public function searchPosts($createdFrom, $createdTo, $category, $city){
    $query = "SELECT 
                    `{$this->posts_table}`.`id`, 
                    `{$this->posts_table}`.`title`,
                    `{$this->posts_table}`.`creation_date` AS `created`,
                    `{$this->posts_table}`.`category`,
                    `{$this->posts_table}`.`description`,
                    `{$this->posts_table}`.`location`,
                    `{$this->posts_table}`.`phone`,
                    `{$this->posts_table}`.`price`,
                    `{$this->posts_table}`.`status`,
                    `{$this->users_table}`.`username` AS `user`,
                    `{$this->categories_table}`.`category` AS `category`,
                    `{$this->cities_table}`.`name` AS `location`
            FROM `{$this->posts_table}`
            INNER JOIN `{$this->users_table}` ON `{$this->posts_table}`.`fk_user`=`{$this->users_table}`.`id`
            INNER JOIN `{$this->categories_table}` ON `{$this->posts_table}`.`category`=`{$this->categories_table}`.`id`
            INNER JOIN `{$this->cities_table}` ON `{$this->posts_table}`.`location`=`{$this->cities_table}`.`id`
            WHERE ('$createdFrom' = '' OR `{$this->posts_table}`.`creation_date` >= '$createdFrom') AND
                  ('$createdTo' = '' OR `{$this->posts_table}`.`creation_date` <= '$createdTo') AND
                  ('$category' = '' OR `{$this->categories_table}`.`category`='$category') AND
                  ('$city' = '' OR `{$this->cities_table}`.`name`='$city')
            ORDER BY `created` DESC";

    $data = Database::select($query);

    $imagesObj = new Image();
    for($i = 0; $i < count($data); $i++){
      $imagesData = $imagesObj->getImages($data[$i]['id']);
      $images = array();
      for($j = 0; $j < count($imagesData); $j++){
        $images[] = $imagesData[$j]['image'];
      }
      $data[$i] += ['images' => $images];
    }

    return $data;
  }

  public function searchOffers($createdFrom, $createdTo, $category, $city, $rating){
    if($rating === ''){
      $rating = 0;
    }

    $sql_query = "SELECT 
                  `{$this->offers_table}`.`id`, 
                  `{$this->offers_table}`.`title`,
                  `{$this->offers_table}`.`creation_date` AS `created`,
                  `{$this->offers_table}`.`category`,
                  `{$this->offers_table}`.`description`,
                  `{$this->offers_table}`.`location`,
                  `{$this->offers_table}`.`phone`,
                  `{$this->offers_table}`.`status`,
                  `{$this->users_table}`.`username` AS `user`,
                  `{$this->categories_table}`.`category` AS `category`,
                  `{$this->cities_table}`.`name` AS `location`,
                  IFNULL(AVG(`{$this->reviews_table}`.`rating`),0) AS `rating`
          FROM `{$this->offers_table}`
          INNER JOIN `{$this->users_table}` ON `{$this->offers_table}`.`fk_user`=`{$this->users_table}`.`id`
          INNER JOIN `{$this->categories_table}` ON `{$this->offers_table}`.`category`=`{$this->categories_table}`.`id`
          INNER JOIN `{$this->cities_table}` ON `{$this->offers_table}`.`location`=`{$this->cities_table}`.`id`
          LEFT JOIN `{$this->reviews_table}` ON `{$this->offers_table}`.`id`=`{$this->reviews_table}`.`fk_offer`
          WHERE ('$createdFrom' = '' OR `{$this->offers_table}`.`creation_date` >= '$createdFrom') AND
                ('$createdTo' = '' OR `{$this->offers_table}`.`creation_date` <= '$createdTo') AND
                ('$category' = '' OR `{$this->categories_table}`.`category`='$category') AND
                ('$city' = '' OR `{$this->cities_table}`.`name`='$city')
          GROUP BY `{$this->offers_table}`.`id`
          HAVING `rating` >= '$rating'
          ORDER BY `created` DESC";

    $data = Database::select($sql_query);
    
    return $data;
  }

  public function extractQueryTerms($query){
    $terms = array();
    if($query !== ""){
      $query = strtolower($query);
      $query = preg_replace("#[[:punct:]]#", " ", $query);
      $terms = explode(" ", $query);
    }

    return $terms;
  }

  public function findTermsInData($data, $terms){
    $results = array();

    for($i = 0; $i < count($data); $i++){
      if(count($terms) === 0){
        $results[] = $data[$i];
      } else {
        for($j = 0; $j < count($terms); $j++){

          $pos = strrpos(strtolower($data[$i]['title']), $terms[$j]);
          if($pos !== FALSE){
            $results[] = $data[$i];
            break;
          }

          $pos = strrpos(strtolower($data[$i]['description']), $terms[$j]);
          if($pos !== FALSE){
            $results[] = $data[$i];
            break;
          }
        }
      }
    }
    return $results;
  }
}

?>