import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Header from './components/Header';
import Footer from './components/Footer';
import Home from './views/Home';
import Login from './views/Login';
import Register from './views/Register';
import ConfirmEmail from './views/ConfirmEmail';
import Error from './views/Error';
import About from './views/About';
import Account from './views/Account';
import UpdateAccount from './views/UpdateAccount';
import ChangePassword from './views/ChangePassword';
import ForgotPassword from './views/ForgotPassword';
import Posts from './views/Posts';
import Post from './views/Post';
import CreatePost from './views/CreatePost';
import EditPost from './views/EditPost';
import Offers from './views/Offers';
import Offer from './views/Offer';
import CreateOffer from './views/CreateOffer';
import EditOffer from './views/EditOffer';
import Search from './views/Search';
import SearchPosts from './views/SearchPosts';
import SearchPostsResults from './views/SearchPostsResults';
import SearchOffers from './views/SearchOffers';
import SearchOffersResults from './views/SearchOffersResults';

class Routes extends Component {
  render() {
    return (
      <Switch>
        <DefaultLayout
          exact path="/"
          component={Home}
          props={this.props.childProps}
        />
        <Route
          exact path="/login"
          render={(props) => <Login {...props} childProps={this.props.childProps} />}
        />
        <Route
          exact path="/register"
          render={(props) => <Register {...props} childProps={this.props.childProps} />}
        />
		    <Route
          exact path="/forgot-Password"
          render={(props) => <ForgotPassword {...props} childProps={this.props.childProps} />}
        />
        <DefaultLayout
          exact path="/confirm/email=:email&token=:token"
          component={ConfirmEmail}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/posts"
          component={Posts}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/posts/create"
          component={CreatePost}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/posts/edit/:id"
          component={EditPost}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/posts/:id"
          component={Post}
          props={this.props.childProps}
        />
		    <DefaultLayout
          exact path="/offers"
          component={Offers}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/offers/create"
          component={CreateOffer}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/offers/edit/:id"
          component={EditOffer}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/offers/:id"
          component={Offer}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/accounts/:username"
          component={Account}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/accounts/:username/update-account"
          component={UpdateAccount}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/accounts/:username/change-password"
          component={ChangePassword}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/about-us"
          component={About}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/search"
          component={Search}
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/search/posts"
          component={SearchPosts}
          props={this.props.childProps}
        />
        <DefaultLayout
          path="/search/posts/results"
          component={SearchPostsResults} 
          props={this.props.childProps}
        />
        <DefaultLayout
          exact path="/search/offers"
          component={SearchOffers}
          props={this.props.childProps}
        />
        <DefaultLayout
          path="/search/offers/results"
          component={SearchOffersResults} 
          props={this.props.childProps}
        />
        <DefaultLayout
          component={Error}
          props={this.props.childProps}
        />
      </Switch>
    );
  }
}

const DefaultLayout = ({ component: Component, props: cProps, ...rest }) => {
  return (
    <Route {...rest} render={matchProps => (
      <div className="defaultLayout">
        <Header {...matchProps} {...cProps} />
        <Component {...matchProps} {...cProps} />
        <Footer />
      </div>
    )} />
  )
};

export default Routes;