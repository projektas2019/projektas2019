import React from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import ErrorIcon from '@material-ui/icons/Error';

function ChangePasswordForm(props) {
  return (
    <div>
      <form>
        <Typography variant="h4" style={{ paddingTop: "20px" }}>Slaptažodžio keitimas</Typography>
        <div>
          {props.data.errorMessage !== "" ?
            <Paper style={{ padding: "10px", backgroundColor: "#F44336", boxShadow: "none", margin: "10px 0", display: "inline-block" }}>
              <ErrorIcon style={{ color: "#FAFAFA", position: "relative", top: "3px" }} /><Typography style={{ color: "#FAFAFA", fontWeight: "bold", display: "inline", paddingLeft: "10px", position: "relative", bottom: "4px" }}>{props.data.errorMessage}</Typography>
            </Paper> :
            <Typography></Typography>}
          <br />
          <TextField
            label="Senas slaptažodis"
            margin="normal"
            variant="outlined"
            name="oldPassword"
            inputProps={{ maxLength: 64 }}
            type="password"
            value={props.data.oldPassword}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            label="Naujas slaptažodis"
            margin="normal"
            variant="outlined"
            inputProps={{ maxLength: 64 }}
            type="password"
            name="newPassword"
            value={props.data.newPassword}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            label="Pakartokite naują slaptažodį"
            margin="normal"
            variant="outlined"
            type="password"
            inputProps={{ maxLength: 64 }}
            name="confirmPassword"
            value={props.data.confirmPassword}
            onChange={props.handleChange}
          />
          <br />
          <Button variant="contained" color="primary" style={{ margin: "20px 0" }} onClick={props.handleClick} disabled={props.data.isUpdating}>
            {props.data.isUpdating && (
              <i
                className="fa fa-refresh fa-spin"
                style={{ marginRight: "5px" }}
              />
            )}
            Keisti slaptažodį</Button>
        </div>
      </form>
    </div>
  );
}

export default ChangePasswordForm;