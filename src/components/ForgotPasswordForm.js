import React from 'react';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import ErrorIcon from '@material-ui/icons/Error';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = ({
  main: {
    margin: "5em auto",
    width: "400px",
    height: "auto"
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: "auto",
    padding: "10px",
    backgroundColor: "#fff",
    border: "1px solid #BDBDBD"
  },
  form: {
    marginTop: "10px"
  },
  submit: {
    margin: "40px 0 20px"
  }
});


function ForgotPasswordForm(props) {
  return (
    <div style={styles.main}>
      <Paper style={styles.paper}>
        <Typography component="h1" variant="h5">
          Slaptažodžio priminimas
        </Typography>
        <form style={styles.form}>
          {props.data.errorMessage !== "" ?
            <Paper style={{ padding: "10px", backgroundColor: "#F44336", boxShadow: "none", marginBottom: "10px" }}>
              <ErrorIcon style={{ color: "#FAFAFA", position: "relative", top: "3px" }} /><Typography style={{ color: "#FAFAFA", fontWeight: "bold", display: "inline", paddingLeft: "10px", position: "relative", bottom: "4px" }}>{props.data.errorMessage}</Typography>
            </Paper> :
            <Typography></Typography>}
          <FormControl style={{ marginBottom: "10px" }} fullWidth >
            <InputLabel htmlFor="email">Elektroninis paštas</InputLabel>
            <Input onChange={props.handleChange} value={props.data.email} inputProps={{ maxLength: 255 }} id="email" name="email" autoComplete="email" />
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={props.handleClick}
            style={styles.submit}
            disabled={props.data.isLoading}
          >
            {props.data.isLoading && (
              <i
                className="fa fa-refresh fa-spin"
                style={{ marginRight: "5px" }}
              />
            )}
            Priminti slaptažodį
          </Button>
          <Link href={"/register"}>Neturite paskyros?</Link>
        </form>
      </Paper>
    </div>
  );
}

export default withStyles(styles)(ForgotPasswordForm);







/*import React from 'react';
import { Link } from 'react-router-dom';
import './ForgotPasswordForm.css';

function ForgotPasswordForm(props) {
  return (
    <div className="forgotPasswordContainer">
      <form>
        <h1>Slaptažodžio priminimas</h1>
        <div className="formContainer">
          <label>Elektroninis paštas</label>
          <input
            type="text"
            maxLength="255"
            name="email"
            value={props.data.email}
            onChange={props.handleChange}
          />
          <button onClick={props.handleClick} disabled={props.data.isLoading}>
            {props.data.isLoading && (
            <i
              className="fa fa-refresh fa-spin"
              style={{ marginRight: "5px" }}
            />
            )}
            Priminti slaptažodį</button>
        </div>
        <Link to="/register" className="link">Registracija</Link>
        <p className="forgotPasswordError"> {props.data.errorMessage}</p>
      </form>
    </div>
  )
}

export default ForgotPasswordForm;*/