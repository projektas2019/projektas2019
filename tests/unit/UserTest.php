<?php

use PHPUnit\Framework\TestCase;
require_once 'api/classes/User.php';
require_once 'api/classes/Database.php';

class UserTest extends TestCase {

  private $db_link;
  private $mysqli;  
  private $usersObj;

  protected function setUp() :void {
    $this->mysqli = new mysqli("localhost", "root", "", "testdb");
    $sql = file_get_contents('tests/unit/testdb.sql');

    $queries = preg_split('/(;)/', $sql, -1, PREG_SPLIT_DELIM_CAPTURE);
    $queriesWithDelimiters = array();
    for ($i = 0, $n = count($queries) - 1; $i < $n; $i += 2) {
      $queriesWithDelimiters[] = $queries[$i].$queries[$i + 1];
    }
    if ($queries[$n] != '') {
      $queriesWithDelimiters[] = $queries[$n];
    }
    
    foreach ($queriesWithDelimiters as $query) {
      $this->mysqli->query($query) or trigger_error($this->mysqli->error);
    }

    Database::$db = 'testdb';
    $this->usersObj = new User();
  } 

  public function testCreateUserSuccess() {
    $message = $this->usersObj->createUser("usernamer", "Password2", "svall@gmail.com");

    $this->assertEquals($message, true);
  }
  public function testCreateUserFailure() {
    $message = $this->usersObj->createUser("usernama", "pass", "svall@gmail.com");

    $this->assertEquals($message, true);
  }

  public function testCreateUserNoUsername() {
    $message = $this->usersObj->createUser("", "Password2", "svall@gmail.com");

    $this->assertEquals($message, true);
  }
  
  public function testCreateUserNoPassword() {
    $message = $this->usersObj->createUser("usernamer", "", "svall@gmail.com");

    $this->assertEquals($message, true);
  }

  public function testCreateUserNoEmail(){
    $message = $this->usersObj->createUser("usernamer", "Password2", "");

    $this->assertEquals($message, true);
  }

  public function testUpdateUserNoUsername() {
    $message = $this->usersObj->updateUser("", "Tomas", "Jonaitis", "Password2", "svall@gmail.com");

    $this->assertEquals($message, 'Telefono numerio formatas turi būti +370******** arba 8********');
  }
  
  public function testUpdateUserNoPassword() {
    $message = $this->usersObj->updateUser("usernamer", "Tomas", "Jonaitis", "", "svall@gmail.com");

    $this->assertEquals($message, 'Telefono numerio formatas turi būti +370******** arba 8********');
  }

  public function testUpdateUserNoEmail(){
    $message = $this->usersObj->updateUser("usernamer", "Tomas", "Jonaitis", "Password2", "");

    $this->assertEquals($message, 'Informacija pakeista');
  }

  public function testUpdateUserNoFirstName(){
    $message = $this->usersObj->updateUser("usernamer", "", "Jonaitis", "Password2", "svall@gmail.com");

    $this->assertEquals($message, 'Telefono numerio formatas turi būti +370******** arba 8********');
  }

  public function testUpdateUserNoLastName(){
    $message = $this->usersObj->updateUser("usernamer", "Tomas", "", "Password2", "svall@gmail.com");

    $this->assertEquals($message, 'Telefono numerio formatas turi būti +370******** arba 8********');
  }

  public function testUpdateUserSuccess(){
    $message = $this->usersObj->createUser("usernamer2", "Password2", "svall@gmail.com");

    $this->assertEquals($message, true);
  }




  protected function tearDown() :void {
    $this->db_link = mysqli_connect('localhost', 'root', '');
    $sql = 'DROP DATABASE testdb';
    mysqli_query($this->db_link, $sql);
    $sql = 'CREATE DATABASE testdb';
    mysqli_query($this->db_link, $sql);
  }
}

?>