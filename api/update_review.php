<?php
  
  include_once 'config.php';
  include_once 'classes\Review.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['reviewEditing'];
  $review = $escaped['review'];
  $rating = $escaped['reviewRating'];
  $title = $escaped['reviewTitle'];

  $reviewsObj = new Review();
  $message = $reviewsObj->updateReview($id, $title, $review, $rating);

  echo json_encode($message);

?>