import React from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import ErrorIcon from '@material-ui/icons/Error';

function OfferForm(props) {
  return (
    <div>
      <form>
        <Typography variant="h4" style={{ paddingTop: "20px" }}>{props.edit ? "Pasiūlymo redagavimas" : "Naujo pasiūlymo kūrimas"}</Typography>
        <div>
          <br />
          <TextField
            label="Pavadinimas *"
            margin="normal"
            variant="outlined"
            name="title"
            inputProps={{ maxLength: 64 }}
            style={{ width: "800px" }}
            type="text"
            value={props.data.title}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            label="Aprašymas *"
            margin="normal"
            variant="outlined"
            name="description"
            multiline
            rows="8"
            inputProps={{ maxLength: 2000 }}
            style={{ width: "800px" }}
            type="text"
            value={props.data.description}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            select
            label="Kategorija *"
            name="category"
            value={props.data.category}
            onChange={props.handleChange}
            SelectProps={{
              native: true
            }}
            margin="normal"
            variant="outlined"
          >
            <option>--Pasirinkite kategoriją--</option>
            {props.data.categories.map(category =>
              <option key={category} value={category}>{category}</option>)}
          </TextField>
          <br />
          <TextField
            select
            label="Miestas *"
            name="location"
            value={props.data.location}
            onChange={props.handleChange}
            SelectProps={{
              native: true
            }}
            margin="normal"
            variant="outlined"
          >
            <option>--Pasirinkite miestą--</option>
            {props.data.cities.map(city =>
              <option key={city} value={city}>{city}</option>)}
          </TextField>
          <br />
          <TextField
            label="Telefono numeris *"
            margin="normal"
            variant="outlined"
            name="phone"
            inputProps={{ maxLength: 12 }}
            type="text"
            value={props.data.phone}
            onChange={props.handleChange}
          />
          <br />
          <Button variant="contained" color="primary" style={{ margin: "20px 0" }} onClick={props.handleClick} disabled={props.data.isCreating}>
            {props.data.isCreating && (
              <i
                className="fa fa-refresh fa-spin"
                style={{ marginRight: "5px" }}
              />
            )}
            {props.edit ? "Išsaugoti" : "Kurti naują pasiūlymą"}</Button>
          <br />
          {props.data.errorMessage !== "" ?
            <Paper style={{ padding: "10px", backgroundColor: "#F44336", boxShadow: "none", margin: "10px 0", display: "inline-block" }}>
              <ErrorIcon style={{ color: "#FAFAFA", position: "relative", top: "3px" }} /><Typography style={{ color: "#FAFAFA", fontWeight: "bold", display: "inline", paddingLeft: "10px", position: "relative", bottom: "4px" }}>{props.data.errorMessage}</Typography>
            </Paper> :
            <Typography></Typography>}
        </div>
      </form>
    </div>
  )
}

export default OfferForm;