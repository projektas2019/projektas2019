import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    color: "#212121",
    textShadow: "none"
  }
});

class NavBar extends Component {
  state = {
    value: 0
  };

  checkLocation(location) {
    if (location['pathname'] === '/') {
      this.setState({ value: 0 })
    } else if (location['pathname'].indexOf('/search') !== -1) {
      this.setState({ value: 1 })
    } else if (location['pathname'].indexOf('/posts') !== -1) {
      this.setState({ value: 2 })
    } else if (location['pathname'].indexOf('/offers') !== -1) {
      this.setState({ value: 3 })
    } else if (location['pathname'] === '/about-us') {
      this.setState({ value: 4 })
    } else {
      this.setState({ value: -1 })
    }
  }

  componentDidMount() {
    this.checkLocation(this.props.childProps.location);
    this.unlisten = this.props.childProps.history.listen((location) => {
      this.checkLocation(location);
    })
  }

  componentWillUnmount() {
    this.unlisten();
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <Tabs onChange={this.handleChange} value={value}>
          <Tab value={0} label="Pradžia" to='/' component={Link} />
          <Tab value={1} label="Paieška" to='/search' component={Link} />
          <Tab value={2} label="Skelbimai" to='/posts' component={Link} />
          <Tab value={3} label="Specialistų pasiūlymai" to='/offers' component={Link} />
          <Tab value={4} label="Kontaktai" to='/about-us' component={Link} />
        </Tabs>
      </div>
    );
  }
}

export default withStyles(styles)(NavBar);