import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { fetchPost } from '../Utils';
import MiniOffer from '../components/MiniOffer';

const styles = {
  button: {
    fontSize: "16px",
    margin: "20px 5px 20px 0"
  }
}

class SearchOffersResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      currentPage: 1,
      offersPerPage: 5,
      isLoading: true
    }
    this.handleClick = this.handleClick.bind(this);
    this.selectElement = this.selectElement.bind(this);
  }

  componentDidMount() {
    const queryParameters = this.props.location.search;
    const params = new URLSearchParams(queryParameters);
    const searchParams = {
      query: params.get('query'),
      createdFrom: params.get('createdFrom'),
      createdTo: params.get('createdTo'),
      category: params.get('category'),
      city: params.get('city'),
      rating: params.get('rating')
    }

    fetchPost('search_offers.php', searchParams)
      .then((result) => {
        this.setState({
          data: result,
          isLoading: false
        })
      })
  }
  handleClick(event) {
    this.setState({
      currentPage: Number(event.currentTarget.id)
    });
    window.scrollTo(0, 0)
  }

  selectElement() {
    if (this.state.isLoading) {
      return <p></p>
    } else {

      const { data, currentPage, offersPerPage } = this.state;
      const indexOfLastOffer = currentPage * offersPerPage;
      const indexOfFirstOffer = indexOfLastOffer - offersPerPage;
      const currentOffers = data.slice(indexOfFirstOffer, indexOfLastOffer);

      const offers = currentOffers.map(item => <MiniOffer key={item.id} data={item} />);
      const pageNumbers = [];
      for (let i = 1; i <= Math.ceil(data.length / offersPerPage); i++) {
        pageNumbers.push(i);
      }

      const renderPageNumbers = pageNumbers.map(number => {
        return (
          <Button
            variant="outlined"
            key={number}
            id={number}
            onClick={this.handleClick}
            style={styles.button}
          >
            {number}
          </Button>
        );
      });
      return (
        <div>
          <Typography variant="h4" style={{ textAlign: "center", padding: "20px 0 10px" }}>{this.state.data.length > 0 ? "Rasta pasiūlymų: " + this.state.data.length : "Pasiūlymų nerasta."}</Typography>
          <ul>
            {renderPageNumbers}
          </ul>
          {offers}
          <ul>
            {renderPageNumbers}
          </ul>
        </div>
      );
    }
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default SearchOffersResults;