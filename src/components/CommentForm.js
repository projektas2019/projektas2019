import React from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import ErrorIcon from '@material-ui/icons/Error';

function CommentForm(props) {
  return (
    <div className="commentFormContainer">
      <Typography variant="h6" style={{ padding: "20px 0 10px" }}>{props.editing ? "" : "Naujas komentaras"}</Typography>
      <form>
        <TextField
          variant="outlined"
          name="comment"
          multiline
          rows="6"
          inputProps={{ maxLength: 2000 }}
          style={{ width: "800px" }}
          type="text"
          value={props.data.comment}
          onChange={props.handleChange}
        />
        <br />
        {props.data.user !== props.childProps.currentUser ?
          <TextField
            label="Siūloma kaina"
            margin="normal"
            variant="outlined"
            name="commentPrice"
            inputProps={{ maxLength: 10 }}
            type="text"
            value={props.data.commentPrice}
            onChange={props.handleChange}
          /> :
          <p></p>
        }
        <Button variant="contained" color="primary" style={{ margin: "10px 0", display: "block" }} onClick={props.createComment}>{props.editing ? "Išsaugoti" : "Pridėti komentarą"}</Button>
        {props.data.commentError !== "" ?
            <Paper style={{ padding: "10px", backgroundColor: "#F44336", boxShadow: "none", margin: "10px 0", display: "inline-block" }}>
              <ErrorIcon style={{ color: "#FAFAFA", position: "relative", top: "3px" }} /><Typography style={{ color: "#FAFAFA", fontWeight: "bold", display: "inline", paddingLeft: "10px", position: "relative", bottom: "4px" }}>{props.data.commentError}</Typography>
            </Paper> :
            <Typography></Typography>}
      </form>
    </div>
  )
}

export default CommentForm;