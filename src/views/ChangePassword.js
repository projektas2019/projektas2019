import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import ChangePasswordForm from '../components/ChangePasswordForm';
import { fetchPost } from '../Utils.js';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
      isUpdating: false,
      errorMessage: "",
      currentUser: ""
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.selectElement = this.selectElement.bind(this);
  }

  async handleClick(event) {
    this.setState({ isUpdating: true })
    event.preventDefault();

    const jsonArray = Object.assign(this.state, {username: this.props.match.params.username});
    await fetchPost('change_password.php', jsonArray)
      .then((result) => {
        if (result !== "Slaptažodis pakeistas") {
          this.setState({
            errorMessage: result
          })
          this.setState({ isUpdating: false });
        } else {
          this.setState({
            errorMessage: ""
          })
          this.setState({ isUpdating: false })
          this.props.history.push('/accounts/' + this.props.match.params.username)
        }
      })
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  selectElement() {
    if (this.props.match.params.username && this.props.match.params.username.length > 0) {
      if (this.props.match.params.username === this.props.currentUser) {
        return <ChangePasswordForm
          handleChange={this.handleChange}
          handleClick={this.handleClick}
          data={this.state}
        />
      }
    }

    return <Typography variant="h5" style={{paddingTop: "20px"}}>Slaptažodį gali keisti tik vartotojas</Typography>
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default ChangePassword;