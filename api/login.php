<?php

  include_once 'config.php';
  include_once 'classes\User.php';
  include_once 'classes\Authentication.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['username'];
  $password = $escaped['password'];

  $message = Authentication::login($username, $password);

  echo json_encode($message);

?>