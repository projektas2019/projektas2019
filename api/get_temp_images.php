<?php
  header("Access-Control-Allow-Origin: *");
  $imageData = array();

  if(isset($_FILES['images'])){
    foreach($_FILES["images"]["tmp_name"] as $key=>$tmp_name) {
      $fileTmpLoc = $_FILES["images"]["tmp_name"][$key];
      $mime = mime_content_type($fileTmpLoc);
      $data = file_get_contents($fileTmpLoc);
      $imageData[] = 'data:' . $mime . ';base64,' . base64_encode($data);
    }
  }
  
  echo json_encode($imageData);
?>