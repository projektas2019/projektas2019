<?php
  
  include_once 'config.php';
  include_once 'classes\Search.php';
  
  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $query = $escaped['query'];
  $createdFrom = $escaped['createdFrom'];
  $createdTo = $escaped['createdTo'];
  $category = $escaped['category'];
  $city = $escaped['city'];
  $rating = $escaped['rating'];

  $searchObj = new Search();
  $data = $searchObj->searchOffers($createdFrom, $createdTo, $category, $city, $rating);
  $terms = $searchObj->extractQueryTerms($query);
  $offers = $searchObj->findTermsInData($data, $terms);

  echo json_encode($offers);

?>

