<?php
  
  include_once 'config.php';
  include_once 'classes\User.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['username'];
  $firstName = $escaped['firstName'];
  $lastName = $escaped['lastName'];
  $location = $escaped['location'];
  $phone = $escaped['phone'];

  $usersObj = new User();
  $message = $usersObj->updateUser($username, $firstName, $lastName, $location, $phone);

  echo json_encode($message);

?>