<?php
  
  include_once 'config.php';
  include_once 'classes\City.php';

  $citiesObj = new City();
  $data = $citiesObj->listCities();

  $cities = array();
  for($i = 0; $i < count($data); $i++){
    $cities[] = $data[$i]['name'];
  }

  echo json_encode($cities);
  
?>