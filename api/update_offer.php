<?php
  
  include_once 'config.php';
  include_once 'classes\Offer.php';
  include_once 'classes\Category.php';
  include_once 'classes\City.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];
  $title = $escaped['title'];
  $description = $escaped['description'];
  $category = $escaped['category'];
  $location = $escaped['location'];
  $phone = $escaped['phone'];

  $offersObj = new Offer();
  $message = $offersObj->updateOffer($id, $title, $description, $category, $location, $phone);
  
  echo json_encode($message);

?>