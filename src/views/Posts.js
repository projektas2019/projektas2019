import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import MiniPost from '../components/MiniPost';
import { fetchPost } from '../Utils.js';

const styles = {
  button: {
    fontSize: "16px", 
    margin: "20px 5px 20px 0"
  }
}

class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      currentPage: 1,
      postsPerPage: 5,
      isLoading: true
    }
    this.handleClick = this.handleClick.bind(this);
    this.selectElement = this.selectElement.bind(this);
  }

  componentWillMount() {
    fetchPost('get_posts.php', {})
      .then((result) => {
        this.setState({
          data: result,
          isLoading: false
        })
      });
  }

  handleClick(event) {
    this.setState({
      currentPage: Number(event.currentTarget.id)
    });
    window.scrollTo(0, 0)
  }

  selectElement() {
    if (this.state.isLoading) {
      return <p></p>
    } else {

      const { data, currentPage, postsPerPage } = this.state;
      const indexOfLastPost = currentPage * postsPerPage;
      const indexOfFirstPost = indexOfLastPost - postsPerPage;
      const currentPosts = data.slice(indexOfFirstPost, indexOfLastPost);

      const posts = currentPosts.map(item => <MiniPost key={item.id} data={item} />);
      const pageNumbers = [];
      for (let i = 1; i <= Math.ceil(data.length / postsPerPage); i++) {
        pageNumbers.push(i);
      }

      const renderPageNumbers = pageNumbers.map(number => {
        return (
          <Button
            variant="outlined"
            key={number}
            id={number}
            onClick={this.handleClick}
            style={styles.button}
          >
            {number}
          </Button>
        );
      });
      return (
        <div>
          <Typography variant="h4" style={{ textAlign: "center", padding: "20px 0 10px" }}>Visi skelbimai</Typography>
          <Button color="primary" variant="contained" href={"posts/create"}>Kurti naują skelbimą</Button>
          <ul>
            {renderPageNumbers}
          </ul>
          {posts}
          <ul>
            {renderPageNumbers}
          </ul>
        </div>
      );
    }
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default Posts;