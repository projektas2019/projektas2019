<?php

include_once 'Database.php';

class User {

  private $users_table = '';
  private $cities_table = '';
  private $offers_table = '';
  private $posts_table = '';
  private $reviews_table = '';
  private $comments_table = '';

  public function __construct() {
    $this->users_table = 'users';
    $this->cities_table = 'cities';
    $this->offers_table = 'offers';
    $this->posts_table = 'posts';
    $this->reviews_table = 'reviews';
    $this->comments_table = 'comments';
  }

  public function createUser($username, $password, $email){
    date_default_timezone_set('Europe/Riga');
    $creation_date = date('Y-m-d H:i:s');

    $password = password_hash($password, PASSWORD_DEFAULT);
    $token = openssl_random_pseudo_bytes(32);
    $token = bin2hex($token);
    $query = "INSERT INTO `{$this->users_table}`
              (
                `username`, 
                `password`, 
                `email`, 
                `email_token`, 
                `role`, 
                `creation_date`
              ) 
              VALUES 
              (
                '$username',
                '$password', 
                '$email', 
                '$token', 
                'unverified', 
                '$creation_date'
              )";

    return Database::query($query);
  }

  public function getUser($username){
    $query = "SELECT 
                     `{$this->users_table}`.`id`,
                     `{$this->users_table}`.`username`,
                     `{$this->users_table}`.`password`,
                     `{$this->users_table}`.`email`,
                     `{$this->users_table}`.`role`,
                     `{$this->users_table}`.`email_token` AS `token`,
                     `{$this->users_table}`.`creation_date`,
                     `{$this->users_table}`.`first_name`,
                     `{$this->users_table}`.`last_name`,
                     `{$this->cities_table}`.`name` AS `city`,
                     `{$this->users_table}`.`phone`
            FROM  `{$this->users_table}`
            LEFT JOIN `{$this->cities_table}` ON `{$this->cities_table}`.`id`=`{$this->users_table}`.`location`
            WHERE `{$this->users_table}`.`username`='$username'";

    $data = Database::select($query);

    if(!$data){
      return false;
    }
    return $data[0];
  }

  public function getUserCity($location){
    $query = "SELECT
                    `id` AS `location`
              FROM `{$this->cities_table}`
              WHERE `name`='$location'";

    $data = Database::select($query);

    if(!$data){
      return NULL;
    }

    return $data[0]['location'];
  }

  public function updateUser($username, $firstName, $lastName, $location, $phone){
    if($location === "" || $location === "--Pasirinkite miestą--"){
      $location = NULL;
    } else {
      $location = $this->getUserCity($location);
    }

    if($phone !== ""){
      if(!(preg_match('/^[+370][0-9]*$/', $phone) && strlen($phone) === 12) &&
         !(preg_match('/^[8][0-9]*$/', $phone) && strlen($phone) === 9)){
        return "Telefono numerio formatas turi būti +370******** arba 8********";
      }
    }
  
    $query = "UPDATE `{$this->users_table}`
              SET
                  `first_name`='$firstName',
                  `last_name`='$lastName',
                  `location`=NULLIF('$location', ''),
                  `phone`='$phone'
              WHERE `username`='$username'";
    
    Database::query($query);

    return 'Informacija pakeista';
  }

  public function changePassword($username, $oldPass, $newPass, $confirmPass){
    $user = $this->getUser($username);

    if(!$user){
      return "Vartotojas neegzistuoja";
    }
    if($oldPass === ""){
      return 'Įveskite seną slaptažodį';
    }
    if(!password_verify($oldPass, $user['password'])){
      return 'Neteisingai suvestas senas slaptažodis';
    }
    if($newPass === ""){
      return 'Įveskite naują slaptažodį';
    }
    if(!(strlen($newPass) > 8 &&
        preg_match('~[0-9]~', $newPass) &&
        preg_match('~[A-Z]~', $newPass))){
      return 'Slaptažodis turi būti bent 8 simbolių ilgio, turėti skaičių ir didžiąją raidę.';
    }
    if($confirmPass === ""){
      return 'Pakartokite slaptažodį';
    }
    if($newPass !== $confirmPass){
      return 'Slaptažodžiai nesutampa';
    }

    $hashed_password = password_hash($newPass, PASSWORD_DEFAULT);

    $query = "UPDATE `{$this->users_table}` 
              SET `{$this->users_table}`.`password`='$hashed_password' 
              WHERE `{$this->users_table}`.`username`='$username'";
    Database::query($query);

    return "Slaptažodis pakeistas";
  }

  public function confirmEmail($email, $token){
    $query = "SELECT 
                    `{$this->users_table}`.`id`,
                    `{$this->users_table}`.`role` 
              FROM  `{$this->users_table}` 
              WHERE `{$this->users_table}`.`email`='$email' AND
                    `{$this->users_table}`.`email_token`='$token'";
    $data = Database::select($query);

    if(!$data){
      return 'Nera tokio';
    }

    $user = $data[0];

    if($user['role'] === 'unverified'){
      $id = $user['id'];

      $query = "UPDATE `{$this->users_table}` 
                SET `{$this->users_table}`.`role`='user' 
                WHERE `{$this->users_table}`.`id`='$id'";

      if (Database::query($query)) {
        return "Patvirtintas";
      } else {
        return "Error: " . Database::error();
      }
    } else {
      return "Jau patvirtintas";
    }
  }

  public function banUser($id){
    $query = "DELETE FROM `{$this->reviews_table}` WHERE `{$this->reviews_table}`.`fk_user`='$id'";
    Database::query($query);
    $query = "DELETE FROM `{$this->comments_table}` WHERE `{$this->comments_table}`.`fk_user`='$id'";
    Database::query($query);
    $query = "DELETE FROM `{$this->posts_table}` WHERE `{$this->posts_table}`.`fk_user`='$id'";
    Database::query($query);
    $query = "DELETE FROM `{$this->offers_table}` WHERE `{$this->offers_table}`.`fk_user`='$id'";
    Database::query($query);
  
    $query = "UPDATE `{$this->users_table}` 
              SET `{$this->users_table}`.`role`='banned' 
              WHERE `{$this->users_table}`.`id`='$id'";
    return Database::query($query);
  }

  public function remindPassword($id){
    if(!$id){
      return 'Vartotojas neegzistuoja';
    }

    $newPassword = openssl_random_pseudo_bytes(16);
    $newPassword = bin2hex($newPassword);
    $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);

    $query = "UPDATE `{$this->users_table}` 
              SET `password`='$hashedPassword'
              WHERE `id`='$id'";

    Database::query($query);

    return 'Naujas slaptažodis: ' . $newPassword;
  }

  public function getUserByEmail($email){
    if($email == ""){
      return 'Įveskite elektroninį paštą';
    }

    $query = "SELECT 
                    `{$this->users_table}`.`id`,
                    `{$this->users_table}`.`username`
              FROM  `{$this->users_table}`
              WHERE `{$this->users_table}`.`email`='$email'";

    $data = Database::select($query);

    if(!$data){
      return false;
    }
    return $data[0];
  }
}


?>