import React, { Component } from 'react';
import Routes from './routes.js';
import Cookies from 'universal-cookie';
import { fetchPost } from './Utils.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggingIn: true,
      isLoggedIn: false,
      currentUser: "",
      userRole: "",
      userEmail: ""
    };
    this.getJWT = this.getJWT.bind(this);
  }

  async componentDidMount() {
    await this.getJWT();

    this.setState({ isLoggingIn: false });
  }

  async getJWT() {
    const cookies = new Cookies();

    await fetchPost('validate_token.php', { jwt: cookies.get('jwt') })
      .then((result) => {
        let userExists = result.username !== undefined;
        this.setState({
          isLoggedIn: userExists,
          currentUser: result.username,
          userRole: result.role,
          userEmail: result.email
        });
      });
  }

  userHasLoggedIn = loggedIn => {
    this.setState({ isLoggedIn: loggedIn });
    this.getJWT();
  }

  render() {
    const childProps = {
      isLoggedIn: this.state.isLoggedIn,
      userHasLoggedIn: this.userHasLoggedIn,
      currentUser: this.state.currentUser,
      userRole: this.state.userRole,
      userEmail: this.state.userEmail
    };

    return (
      !this.state.isLoggingIn &&
      <div className="mainContainer">
        <Routes childProps={childProps} />
      </div>
    );
  }
}

export default App;