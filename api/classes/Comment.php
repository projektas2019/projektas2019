<?php

include_once 'Database.php';

class Comment {

  private $comments_table = '';
  private $users_table = '';

  public function __construct() {
    $this->comments_table = 'comments';
    $this->users_table = 'users';
  }

  public function createComment($user, $post, $comment, $price){
    if($comment === ""){
      return"Įveskite komentarą";
    }
  
    if($price === ""){
      $price = NULL;
    } else if (!is_numeric($price) || $price <= 0){
      return "Netinkama kaina";
    } else {
      number_format($price, 2, '.', '');
    }
  
    date_default_timezone_set('Europe/Riga');
    $creation_date = date('Y-m-d H:i:s');
  
    $query = "INSERT INTO `{$this->comments_table}` 
              (
                comment, 
                price, 
                creation_date, 
                edit_date, 
                fk_user, 
                fk_post
              ) 
              VALUES 
              (
                '$comment', 
                NULLIF('$price', 0.00), 
                '$creation_date', 
                NULL, 
                '$user', 
                '$post'
              )";
              
    if(Database::query($query)){
      return 'Comment created';
    } else {
      return 'Įvyko klaida kuriant komentarą';
    }

  }

  public function listComments($id){
    $query = "SELECT 
                      `{$this->comments_table}`.`id`,
                      `{$this->comments_table}`.`comment`,
                      `{$this->comments_table}`.`price`,
                      `{$this->comments_table}`.`creation_date` AS `created`,
                      `{$this->comments_table}`.`edit_date` AS `edited`,
                      `{$this->users_table}`.`username` AS `user`
              FROM `{$this->comments_table}`
              INNER JOIN `{$this->users_table}` ON `{$this->users_table}`.`id`=`{$this->comments_table}`.`fk_user`
              WHERE `{$this->comments_table}`.`fk_post`='$id'
              ORDER BY `{$this->comments_table}`.`creation_date` ASC";

    $data = Database::select($query);

    return $data;
  }

  public function updateComment($id, $comment, $price){
    if($comment === ""){
      return "Įveskite komentarą";
    }
    if($price === ""){
      $price = NULL;
    } else if (!is_numeric($price) || $price <= 0){
      return "Netinkama kaina";
    } else {
      number_format($price, 2, '.', '');
    }

    $query = "UPDATE `{$this->comments_table}`
              SET
                  `comment`='$comment',
                  `price`=NULLIF('$price', 0.00)
              WHERE `id`='$id'";
    
    Database::query($query);

    return 'Comment updated';
  }

  public function deleteComment($id){
    $query = "DELETE FROM `{$this->comments_table}` 
              WHERE id='$id'";
    return Database::query($query);
  }
}


?>