import React from 'react';
import { Link } from 'react-router-dom';
import NavBar from './NavBar';
import UserControl from './UserControl';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';

const styles = ({
  root: {
    backgroundColor: "#FFA726",
    padding: "20px"
  },
  toolbar: {
    backgroundColor: "#f3e8e8",
    minHeight: "0",
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingRight: "0"
  },
  image: {
    margin: "0 auto",
    width: "600px",
    objectFit: "contain"
  },
  registerButton: {
    backgroundColor: "#267EFF",
    color: "#FAFAFA",
    fontWeight: "bold",
    margin: "5px"
  },
  loginButton: {
    backgroundColor: "#19E663",
    color: "#FAFAFA",
    fontWeight: "bold",
    margin: "5px"
  }
});

function Header(props) {
  return (
    <header>
      <div className="authLinks">
        {props.isLoggedIn
          ? <UserControl childProps={props} />
          : <AppBar position="static" style={{boxShadow: "none"}}>
              <Toolbar style={styles.toolbar}>
                <Button
                  style={styles.registerButton}
                  variant="contained"
                  to="/register"
                  component={Link}
                >
                  Registracija
                  </Button>
                <Button
                  style={styles.loginButton}
                  variant="contained"
                  to="/login"
                  component={Link}
                >
                  Prisijungimas
                </Button>
              </Toolbar>
            </AppBar>}
      </div>

      <AppBar position="static" fullwidth="true" style={styles.root} >
        <img src={require("../images/logo.png")} alt="" style={styles.image} />
      </AppBar>

      <NavBar childProps={props} />
    </header>
  );
}

export default Header;