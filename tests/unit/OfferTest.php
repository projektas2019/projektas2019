<?php

use PHPUnit\Framework\TestCase;
require_once 'api/classes/Offer.php';
require_once 'api/classes/Database.php';

class OfferTest extends TestCase {

  private $db_link;
  private $mysqli;
  private $offersObj;

  protected function setUp() :void {
    $this->mysqli = new mysqli("localhost", "root", "", "testdb");
    $sql = file_get_contents('tests/unit/testdb.sql');

    $queries = preg_split('/(;)/', $sql, -1, PREG_SPLIT_DELIM_CAPTURE);
    $queriesWithDelimiters = array();
    for ($i = 0, $n = count($queries) - 1; $i < $n; $i += 2) {
      $queriesWithDelimiters[] = $queries[$i].$queries[$i + 1];
    }
    if ($queries[$n] != '') {
      $queriesWithDelimiters[] = $queries[$n];
    }
    
    foreach ($queriesWithDelimiters as $query) {
      $this->mysqli->query($query) or trigger_error($this->mysqli->error);
    }

    Database::$db = 'testdb';
    $this->offersObj = new Offer();
  }
  public function testCreateOfferSuccess() {
    $message = $this->offersObj->createOffer(1, "title", "description", 5, 1, 867288555);

    $this->assertEquals($message, 'Offer created');
  }
  
  public function testCreateOfferFailure() {
    $message = $this->offersObj->createOffer(1, "title", "description", 5, 300, 867288555);

    $this->assertEquals($message, 'Įvyko klaida kuriant pasiūlymą');
  }

  public function testCreateOfferNoTitle() {
    $message = $this->offersObj->createOffer(1, "", "description", 5, 1, 867288555);

    $this->assertEquals($message, 'Įveskite pasiūlymo pavadinimą');
  }

  public function testCreateOfferNoDescription() {
    $message = $this->offersObj->createOffer(1, "title", "", 5, 1, 867288555);

    $this->assertEquals($message, 'Įveskite pasiūlymo aprašymą');
  }

  public function testCreateOfferNoCategory(){
    $message = $this->offersObj->createOffer(1, "title", "description", "", 1, 867288555);

    $this->assertEquals($message, 'Įvyko klaida kuriant pasiūlymą');
  } 

  public function testCreateOfferNoCity(){
    $message = $this->offersObj->createOffer(1, "title", "description", 5, "", 867288555);

    $this->assertEquals($message, 'Įvyko klaida kuriant pasiūlymą');
  } 

  public function testCreateOfferNoPhone(){
    $message = $this->offersObj->createOffer(1, "title", "description", 5, 1, "");

    $this->assertEquals($message, 'Įveskite savo telefono numerį');
  } 

  public function testUpdateOfferNoTitle(){
    $message = $this->offersObj->updateOffer(1, "", "description", 5, 1, 867288555);

    $this->assertEquals($message, "Įveskite pasiūlymo pavadinimą");
  }

  public function testUpdateOfferNoDescription(){
    $message = $this->offersObj->updateOffer(1, "title", "", 5, 1, 867288555);

    $this->assertEquals($message, "Įveskite pasiūlymo aprašymą");
  }

  public function testUpdateOfferSuccess(){
    $message = $this->offersObj->updateOffer(1, "new title", "new desc", 5, 1, 867288555);

    $this->assertEquals($message, "Offer updated");
  }

  public function testDeleteOfferFailure(){
    $success = $this->offersObj->deleteOffer(5);

    $this->assertEquals($success, false);
  }
  public function testUpdateOfferNoCategory(){
    $message = $this->offersObj->updateOffer(1, "title", "description", "", 1, 867288555);

    $this->assertEquals($message, 'Pasirinkite kategoriją');
  } 

  public function testUpdateOfferNoCity(){
    $message = $this->offersObj->updateOffer(1, "title", "description", 5, "", 867288555);

    $this->assertEquals($message, 'Pasirinkite miestą');
  } 

  public function testUpdateOfferNoPhone(){
    $message = $this->offersObj->updateOffer(1, "title", "description", 5, 1, "");

    $this->assertEquals($message, 'Įveskite savo telefono numerį');
  } 





  protected function tearDown() :void {
    $this->db_link = mysqli_connect('localhost', 'root', '');
    $sql = 'DROP DATABASE testdb';
    mysqli_query($this->db_link, $sql);
    $sql = 'CREATE DATABASE testdb';
    mysqli_query($this->db_link, $sql);
  }
}

?>