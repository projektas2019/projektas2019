<?php

include_once 'Database.php';

class Category {

  private $categories_table = '';

  public function __construct() {
    $this->categories_table = 'categories';
  }

  public function getCategory($category){
    if($category === "" || $category === "--Pasirinkite kategoriją--"){
      return NULL;
    }

    $query = "SELECT 
                    `{$this->categories_table}`.`id`
              FROM  `{$this->categories_table}`
              WHERE `{$this->categories_table}`.`category`='$category'";

    $data = Database::select($query);

    if(!$data){
      return false;
    }

    return $data[0];
  }

  public function listCategories(){
    $query = "SELECT 
                      id,
                      category
              FROM `{$this->categories_table}` 
              ORDER BY category";
    $data = Database::select($query);

    return $data;
  }
}

?>