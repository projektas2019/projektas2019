import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AccountContainer from '../components/AccountContainer';
import AccountInfo from '../components/AccountInfo';
import { fetchPost } from '../Utils.js';

const styles = ({
  banButton: {
    backgroundColor: "#B71C1C",
    color: "#FAFAFA",
    fontWeight: "bold",
    marginTop: "20px"
  }
});

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      role: "",
      created: "",
      firstName: "",
      lastName: "",
      location: "",
      phone: "",
      isLoading: true
    }
    this.selectElement = this.selectElement.bind(this);
    this.banUser = this.banUser.bind(this);
    this.getUser = this.getUser.bind(this);
  }

  async getUser(){
    await fetchPost('get_user.php', { username: this.props.match.params.username })
    .then((result) => {
      this.setState({
        username: result.username,
        email: result.email,
        role: result.role,
        created: result.created,
        firstName: result.firstName,
        lastName: result.lastName,
        location: result.location,
        phone: result.phone
      })
    });

  }
  async componentDidMount() {
    await this.getUser();

    this.setState({ isLoading: false })
  }

  selectElement() {
    if (this.state.isLoading) {
      return <div></div>
    }
    if(this.state.role === "banned"){
      return <Typography variant="h5" style={{paddingTop: "20px"}}>Vartotojas užblokuotas.</Typography> 
    }
    if (this.state.username && this.state.username.length > 0) {
      if (this.state.username === this.props.currentUser) {
        return <AccountContainer data={this.state} />
      }
      return <div>
        <AccountInfo data={this.state} />
        {this.props.userRole === "admin" && this.state.role !== "banned"? 
          <Button variant="contained" style={styles.banButton} onClick={this.banUser}>Užblokuoti vartotoją</Button> :
          <p></p>}
      </div>
    }
    return <Typography variant="h5" style={{paddingTop: "20px"}}>Vartotojas neegzistuoja.</Typography>
  }

  banUser() {
    fetchPost('ban_user.php', this.state)
      .then((result) => {
        if(result === "Vartotojas užblokuotas"){
         this.getUser();
        }
      });
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default Account;