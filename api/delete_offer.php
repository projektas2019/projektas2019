<?php
  
  include_once 'config.php';
  include_once 'classes\Offer.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];

  $offersObj = new Offer();
  $offersObj->deleteOffer($id);

  echo json_encode('Deleted offer');
  
?>