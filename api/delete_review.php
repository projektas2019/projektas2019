<?php
  
  include_once 'config.php';
  include_once 'classes\Review.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];

  $reviewsObj = new Review();
  $reviewsObj->deleteReview($id);

  echo json_encode('Deleted review');
  
?>