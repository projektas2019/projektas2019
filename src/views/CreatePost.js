import React, { Component } from 'react';
import PostForm from '../components/PostForm';
import { getCategories, getCities, fetchPost } from '../Utils.js';

class CreatePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      location: "--Pasirinkite miestą--",
      category: "--Pasirinkite kategoriją--",
      phone: "",
      price: "",
      categories: [],
      cities: [],
      errorMessage: "",
      images: [],
      imageNames: [],
      imagesMessage: "Nėra pasirinktų nuotraukų",
      isLoading: true,
      isCreating: false,
      tempImages: []
    };
    this.selectElement = this.selectElement.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.uploadPhotos = this.uploadPhotos.bind(this);
  }

  async uploadPhotos(event) {
    event.preventDefault();

    const files = Array.from(event.target.files);

    if (files.length > 10) {
      this.setState({ imagesMessage: "Daugiausiai leistina įkelti 10 nuotraukų" });
      return;
    }

    for (let i = 0; i < files.length; ++i) {
      if (files[i].size > 16777215) {
        this.setState({ imagesMessage: "Failas per didelis" });
        return;
      }
    }

    const types = ['image/png', 'image/jpeg'];
    for (let i = 0; i < files.length; ++i) {
      if (types.every(type => files[i].type !== type)) {
        this.setState({ imagesMessage: "Netinkamas failo formatas" });
        return;
      }
    }

    // failai pervadinami atsitiktiniais simboliais
    for (let i = 0; i < files.length; ++i) {
      files[i].renameFile = Math.random().toString(36).substr(2, 34) + '.' + files[i].name.substring(files[i].name.lastIndexOf('.') + 1, files[i].name.length);
    }

    const formData = new FormData();
    files.forEach((file) => {
      formData.append("images[]", file);
      formData.append("imageNames[]", file.renameFile);
    })

    // fileNames = pervadintų failų vardų masyvas
    const fileNames = files.map((file) => file.renameFile);

    this.setState({
      images: formData,
      imageNames: fileNames,
      imagesMessage: " Pasirinkta nuotraukų: " + files.length
    });

    fetch('http://localhost/repair-website/api/get_temp_images.php', {
      method: 'POST',
      body: formData
    })
      .then(result => result.json())
      .then(resultJson => {
        this.setState({
          tempImages: resultJson
        })
      })
  }

  async componentDidMount() {
    await getCategories().then((result) => {
      this.setState({ categories: result })
    });

    await getCities().then((result) => {
      this.setState({ cities: result })
    });

    if (this.props.edit) {
      const imageNames = this.props.data.images.map((image) => image.image);
      const images = this.props.data.images.map((image) => '/images/uploads/' + image.image);

      await this.setState({
        title: this.props.data.title,
        description: this.props.data.description,
        category: this.props.data.category,
        location: this.props.data.location,
        phone: this.props.data.phone,
        price: this.props.data.price == null ? "" : this.props.data.price,
        tempImages: images,
        imageNames: imageNames,
        imagesMessage: images.length !== 0 ? " Pasirinkta nuotraukų: " + images.length : "Nėra pasirinktų nuotraukų"
      })
    }

    this.setState({ isLoading: false });
  }

  async handleClick(event) {
    event.preventDefault();

    this.setState({ isCreating: true });

    fetch('http://localhost/repair-website/api/upload_images.php', {
      method: 'POST',
      body: this.state.images
    })
      .catch((error) => {
        console.log(error);
      })

    let url;
    let message;
    let jsonArray;
    let redirect;

    if (!this.props.edit) {
      jsonArray = Object.assign(this.state, { user: this.props.currentUser });
      message = "Post created";
      url = "create_post.php";
      redirect = this.props.history;
    } else {
      jsonArray = Object.assign(this.state, { id: this.props.id });
      message = "Post updated";
      url = "update_post.php";
      redirect = this.props.childProps.history
    }

    fetchPost(url, jsonArray)
      .then((result) => {
        if (result !== message) {
          this.setState({
            errorMessage: result,
            isCreating: false
          });
        } else {
          this.setState({
            errorMessage: "",
            isCreating: false
          });
          redirect.push('/posts');
        }
      })
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  selectElement() {
    if (this.state.isLoading) {
      return <p></p>
    }
    if (!this.props.edit) {
      if (!this.props.isLoggedIn) {
        return <p>Turite būti prisijungęs, kad galėtumėte kurti naują skelbimą.</p>
      }
      if (this.props.userRole === 'unverified') {
        return <p>Prieš kuriant skelbimą, patvirtinkite savo elektroninį paštą</p>
      }
    } else {
      if (this.props.childProps.currentUser !== this.props.data.user) {
        return <p>Skelbimą redaguoti gali tik jį sukūręs vartotojas.</p>
      }
    }

    return <PostForm
      handleChange={this.handleChange}
      handleClick={this.handleClick}
      uploadPhotos={this.uploadPhotos}
      data={this.state}
      edit={this.props.edit}
    />
  }

  render() {
    return (
      <main className="postForm">
        {this.selectElement()}
      </main>
    );
  }
}

export default CreatePost;