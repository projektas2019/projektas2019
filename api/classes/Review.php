<?php

include_once 'Database.php';

class Review {

  private $reviews_table = '';
  private $users_table = '';

  public function __construct() {
    $this->reviews_table = 'reviews';
    $this->users_table = 'users';
  }

  public function createReview($user, $offer, $title, $review, $rating){
    if($title === ""){
      return "Įveskite pavadinimą";
    }
    if($review === ""){
      return "Įveskite atsiliepimą";
    }
    if($rating === 0){
      return "Pasirinkite įvertinimą";
    }
  
    date_default_timezone_set('Europe/Riga');
    $creation_date = date('Y-m-d H:i:s');
  
    $query = "INSERT INTO `{$this->reviews_table}`
            (
              `title`, 
              `creation_date`, 
              `description`, 
              `rating`, 
              `fk_user`, 
              `fk_offer`
            )
            VALUES 
            (
              '$title', 
              '$creation_date', 
              '$review', 
              '$rating', 
              '$user', 
              '$offer'
            )";

    if(Database::query($query)){
      return 'Review created';
    } else {
      return 'Įvyko klaida kuriant atsiliepimą';
    }
  }

  public function listReviews($id){
    $query = "SELECT 
                      `{$this->reviews_table}`.`id`,
                      `{$this->reviews_table}`.`title`,
                      `{$this->reviews_table}`.`description`,
                      `{$this->reviews_table}`.`creation_date` AS `created`,
                      `{$this->reviews_table}`.`rating`,
                      `{$this->users_table}`.`username` AS `user`
              FROM `{$this->reviews_table}`
              INNER JOIN `{$this->users_table}` ON `{$this->users_table}`.`id`=`{$this->reviews_table}`.`fk_user`
              WHERE `{$this->reviews_table}`.`fk_offer`='$id'
              ORDER BY `{$this->reviews_table}`.`creation_date` ASC";

    $data = Database::select($query);

    return $data;
  }

  public function updateReview($id, $title, $review, $rating){
    if($title === ""){
      return "Įveskite pavadinimą";
    }
    if($review === ""){
      return "Įveskite atsiliepimą";
    }
    if($rating === 0){
      return "Pasirinkite įvertinimą";
    }
  
    $query = "UPDATE `{$this->reviews_table}` 
              SET 
                  `title`='$title',
                  `description`='$review', 
                  `rating`='$rating' 
              WHERE `id`='$id'";
  
    Database::query($query);

    return 'Review updated';
  }

  public function deleteReview($id){
    $query = "DELETE FROM `{$this->reviews_table}` 
              WHERE id='$id'";
    return Database::query($query);
  }
}


?>