-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2019 at 12:38 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `repair_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` char(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`) VALUES
(1, 'Kompiuteriai'),
(2, 'Telefonai'),
(3, 'Televizoriai'),
(4, 'Elektronika'),
(5, 'Mikrobangų krosnelės'),
(6, 'Monitoriai'),
(7, 'Virduliai'),
(8, 'Šaldytuvai'),
(9, 'Baldai'),
(10, 'Programinė įranga');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Akmenė'),
(2, 'Alytus'),
(3, 'Anykščiai'),
(4, 'Ariogala'),
(5, 'Birštonas'),
(6, 'Biržai'),
(7, 'Druskininkai'),
(8, 'Elektrėnai'),
(9, 'Gargždai'),
(10, 'Ignalina'),
(11, 'Jonava'),
(12, 'Joniškis'),
(13, 'Juodkrantė'),
(14, 'Jurbarkas'),
(15, 'Kaišiadorys'),
(16, 'Kalvarija'),
(17, 'Kaunas'),
(18, 'Kazlų Rūda'),
(19, 'Kelmė'),
(20, 'Kėdainiai'),
(21, 'Klaipėda'),
(22, 'Kretinga'),
(23, 'Kupiškis'),
(24, 'Kuršėnai'),
(25, 'Lazdijai'),
(26, 'Lentvaris'),
(27, 'Marijampolė'),
(28, 'Mažeikiai'),
(29, 'Melnragė'),
(30, 'Molėtai'),
(31, 'Naujoji Akmenė'),
(32, 'Neringa'),
(33, 'Nida'),
(34, 'Pagėgiai'),
(35, 'Pakruojis'),
(36, 'Palanga'),
(37, 'Panevėžys'),
(38, 'Pasvalys'),
(39, 'Plungė'),
(40, 'Prienai'),
(41, 'Radviliškis'),
(42, 'Raseiniai'),
(43, 'Rietavas'),
(44, 'Rokiškis'),
(45, 'Skuodas'),
(46, 'Šakiai'),
(47, 'Šalčininkai'),
(48, 'Šiauliai'),
(49, 'Šilalė'),
(50, 'Šilutė'),
(51, 'Širvintos'),
(52, 'Švenčionys'),
(53, 'Šventoji'),
(54, 'Tauragė'),
(55, 'Telšiai'),
(56, 'Trakai'),
(57, 'Ukmergė'),
(58, 'Utena'),
(59, 'Varėna'),
(60, 'Vievis'),
(61, 'Vilkaviškis'),
(62, 'Vilnius'),
(64, 'Visaginas');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `price` float(10,2) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `edit_date` datetime DEFAULT NULL,
  `fk_user` int(11) NOT NULL,
  `fk_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `price`, `creation_date`, `edit_date`, `fk_user`, `fk_post`) VALUES
(11, 'Tinka', 100.00, '2019-05-02 20:24:09', NULL, 20, 198),
(12, 'fhgdn', 5.00, '2019-05-02 20:51:12', NULL, 20, 198),
(13, 'fhgdn', 5.00, '2019-05-02 20:51:17', NULL, 20, 198),
(14, 'fhgdn', 5.00, '2019-05-02 20:51:59', NULL, 20, 198),
(15, 'fhgdn', 10.00, '2019-05-02 20:52:22', NULL, 20, 198),
(16, 'rtsdeg', NULL, '2019-05-02 20:53:50', NULL, 20, 198),
(17, 'fhtgj', 10.00, '2019-05-02 20:56:20', NULL, 20, 198),
(18, 'uyg', NULL, '2019-05-02 20:56:42', NULL, 20, 198),
(19, 'pkl', 10.50, '2019-05-02 20:58:59', NULL, 20, 198),
(20, 'dsafta', 21.00, '2019-05-02 20:59:10', NULL, 20, 198),
(21, 'uyjg', 10.00, '2019-05-02 20:59:15', NULL, 20, 198),
(22, 'adsf', NULL, '2019-05-02 22:06:49', NULL, 1, 198),
(28, 'ufjftfrdegh', NULL, '2019-05-03 00:10:33', NULL, 1, 198),
(29, 'ufjfj', NULL, '2019-05-03 00:10:39', NULL, 1, 198);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image`, `fk_post`) VALUES
(2244, 'g044zcd08mc.jpg', 196),
(2245, 'uq1zu57lbf.png', 198),
(2246, 'c7hswq6o946.jpeg', 198),
(2247, 'wwixnftcuv.jpg', 198);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `title` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creation_date` datetime NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `location` int(11) NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fk_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `title`, `creation_date`, `category`, `description`, `location`, `phone`, `status`, `fk_user`) VALUES
(3, 'Taisome kompiuterius', '2019-04-17 00:00:00', 1, 'Vilniuje taisome kompiuterius už padorią kainą ir nemokamai prie to pačio perrašinėjame Windows.', 2, '2147483647', 'open', 1),
(4, 'Taisau elektroninius prietaisus', '2019-04-17 00:00:00', 4, 'Priklausomai nuo gedimo tokia ir kaina.', 1, '867577944', 'open', 1),
(5, 'Telefonų taisymas', '2019-04-17 01:29:35', 2, 'Nebrangiai taisome telefonus.', 1, '867994124', 'open', 1),
(6, 'Windows perrašymas nemokamai', '2019-04-17 01:35:31', 10, 'Už ačiū.', 1, '867942100', 'open', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creation_date` datetime NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `location` int(11) NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `price` float(10,2) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fk_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `creation_date`, `category`, `description`, `location`, `phone`, `price`, `status`, `fk_user`) VALUES
(196, 'Televizorius', '2019-05-02 01:23:56', 3, 'Sugedo televizorius. Išsijungia kas 30 min.', 30, '867586756', 50.00, 'open', 1),
(197, 'Reikia perrašyti Windows', '2019-05-02 01:24:35', 10, 'Pats nemoku.', 10, '+37069842001', 20.00, 'open', 1),
(198, 'Dulkių siurblys neįsijungia', '2019-05-02 01:26:07', 4, 'Neįsijungia.', 17, '869991042', NULL, 'open', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `title` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creation_date` date NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 NOT NULL,
  `rating` smallint(3) NOT NULL,
  `fk_offer` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `role` varchar(10) CHARACTER SET utf8 NOT NULL,
  `email_token` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creation_date` datetime NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`, `email_token`, `creation_date`, `first_name`, `last_name`, `location`, `phone`) VALUES
(1, 'adminas', '$2y$10$Ha.tEncgWeJwd4eXFgNnAunTm3WDJl7oqMhwPjeSq4Qy61p/DoLvm', 'pupelislukas053@gmail.com', 'admin', '8e1648b1f08016461a8cd06ee25c5f8585e0cf283f9767b92205fbd98e48487f', '2019-02-10 08:04:03', 'Lukas', 'Pupelis', 27, '+37067586765'),
(18, 'naujas', '$2y$10$nhUic7qlvBDmMFW.vAFbpOibizLK/zLPUEq1twrKMEE0da83DLqW2', 'roe@as.sa', 'unverified', 'eb9c9d8b3b8c27b767a50fc92f5f655df5bdd58de4e44714e3df683d9718dfcf', '2019-04-16 15:44:47', 'Lukas', 'Pupelis', 6, '867586765'),
(19, 'LukasPupelis123', '$2y$10$O4TbGKmcGD5rna/qN0SA5eMVgtp4wT.d77fx5g7k9k1Hkrgt5OP7.', 'asfda@asfdas.c', 'unverified', '81f3eb533416f90a3da1c1ae6094d598f206328256dd67d0c059268d05afac0b', '2019-04-27 18:11:10', NULL, NULL, NULL, NULL),
(20, 'LukasPupelis1', '$2y$10$9T4g7v9q2AAHJsez51QJ0u3OxSJuhGcgOs91GycC0YHUMJPXDSSqW', 'pupelis.lukas@gmail.com', 'user', '121cfd7f6d013120c73efc118d602eab51c68002e205e72950d1003e2511a10e', '2019-04-27 23:00:29', NULL, NULL, NULL, NULL),
(21, 'naujasaaa', '$2y$10$H4G1sKFqe4Bh.QFUl1KywOjyYpgmS1yuAvTGeNbdETveCX/t3psTu', 'Naujas@gma.c', 'unverified', '53e08277e3f38edf9548d7a8c37317376c52a1ff6698e2bee851f2744c06747c', '2019-04-28 00:22:19', NULL, NULL, NULL, NULL),
(22, 'Lukas', '$2y$10$MlkCnyv9lzIHMn1JeWMOP.peNbJ87fjWV.R10.DPxdXP5xo0SkzAe', 'asdfjkn@gdas.as', 'unverified', '0fa21f1c498b54c89cb569f495f1c3af06485402a5df136815ec5a60463794be', '2019-05-02 22:12:09', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkc_commenter` (`fk_user`),
  ADD KEY `fkc_page` (`fk_post`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkc_post` (`fk_post`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`),
  ADD KEY `location` (`location`),
  ADD KEY `fkc_repairman` (`fk_user`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`),
  ADD KEY `location` (`location`),
  ADD KEY `fkc_author` (`fk_user`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkc_offer` (`fk_offer`),
  ADD KEY `fkc_reviewer` (`fk_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location` (`location`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2248;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fkc_commenter` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fkc_page` FOREIGN KEY (`fk_post`) REFERENCES `posts` (`id`);

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `fkc_post` FOREIGN KEY (`fk_post`) REFERENCES `posts` (`id`);

--
-- Constraints for table `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`category`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `fkc_repairman` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `location_ibfk_1` FOREIGN KEY (`location`) REFERENCES `cities` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `fkc_author` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `location_ibfk_2` FOREIGN KEY (`location`) REFERENCES `cities` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `fkc_offer` FOREIGN KEY (`fk_offer`) REFERENCES `offers` (`id`),
  ADD CONSTRAINT `fkc_reviewer` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `location_ibfk_3` FOREIGN KEY (`location`) REFERENCES `cities` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
