<?php
  
  include_once 'config.php';
  include_once 'classes\Post.php';
  include_once 'classes\Image.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];

  $postsObj = new Post();
  $postData = $postsObj->getPost($id);

  $imagesObj = new Image();
  $imagesData = $imagesObj->getImages($id);

  $images = array();
  for($i = 0; $i < count($imagesData); $i++){
    $images[] = array(
      'id' => $imagesData[$i]['id'],
      'image' => $imagesData[$i]['image']);
  }

  $post = array(
    'title' => $postData['title'],
    'created' => $postData['created'],
    'category' => $postData['category'],
    'description' => $postData['description'],
    'location' => $postData['location'],
    'phone' => $postData['phone'],
    'price' => $postData['price'],
    'status' => $postData['status'],
    'user' => $postData['user'],
    'images' => $images
  );

  echo json_encode($post);

?>

