<?php
  
  include_once 'config.php';
  include_once 'classes\User.php';
  include_once 'classes\Email.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $email = $escaped['email'];

  $usersObj = new User();
  $user = $usersObj->getUserByEmail($email);

  if($user === 'Įveskite elektroninį paštą'){
    echo json_encode($user);
    return;
  }

  $message = $usersObj->remindPassword($user['id']);

  if(strpos($message, 'Naujas slaptažodis:') > -1){
    $emailObj = new Email();
    $subject = "Slaptažodžio priminimas";
    $body = "
	  $message
      ";
    $message = $emailObj->sendEmail($email, $user['username'], $subject, $body);
    if($message === 'Email sent'){
      $message = 'Password changed';
    }
  }

  echo json_encode($message);
      
?>