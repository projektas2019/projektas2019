import React from 'react';
import Link from '@material-ui/core/Link';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import ErrorIcon from '@material-ui/icons/Error';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = ({
  main: {
    margin: "5em auto",
    width: "400px",
    height: "auto"
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: "auto",
    padding: "10px",
    backgroundColor: "#fff",
    border: "1px solid #BDBDBD"
  },
  avatar: {
    backgroundColor: "#E91E63"
  },
  form: {
    marginTop: "10px"
  },
  submit: {
    margin: "40px 0 20px"
  }
});


function LoginForm(props) {
  return (
    <div style={styles.main}>
      <Paper style={styles.paper}>
        <Avatar style={styles.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Prisijungimas
        </Typography>
        <form style={styles.form}>
        {props.data.errorMessage !== "" ?
            <Paper style={{ padding: "10px", backgroundColor: "#F44336", boxShadow: "none", marginBottom: "10px" }}>
              <ErrorIcon style={{ color: "#FAFAFA", position: "relative", top: "3px" }} /><Typography style={{ color: "#FAFAFA", fontWeight: "bold", display: "inline", paddingLeft: "10px", position: "relative", bottom: "4px" }}>{props.data.errorMessage}</Typography>
            </Paper> :
            <Typography></Typography>}
          <FormControl style={{marginBottom: "10px"}} fullWidth >
            <InputLabel htmlFor="username">Vartotojo vardas</InputLabel>
            <Input onChange={props.handleChange} value={props.data.username} inputProps={{maxLength: 30}} id="username" name="username" autoComplete="username" autoFocus />
          </FormControl>
          <FormControl style={{marginBottom: "10px"}} fullWidth>
            <InputLabel htmlFor="password">Slaptažodis</InputLabel>
            <Input onChange={props.handleChange} value={props.data.password} inputProps={{maxLength: 64}} name="password" type="password" id="password" autoComplete="current-password" />
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={props.handleClick}
            style={styles.submit}
            disabled={props.data.isLoading}
          >
            {props.data.isLoading && (
              <i
                className="fa fa-refresh fa-spin"
                style={{ marginRight: "5px" }}
              />
            )}
            Prisijungti
          </Button>
            <Link href={"/register"}>Neturite paskyros?</Link>
            <br />
            <Link href={"/forgot-password"}>Pamiršote slaptažodį?</Link>
          
        </form>
      </Paper>
    </div>
  );
}

export default withStyles(styles)(LoginForm);