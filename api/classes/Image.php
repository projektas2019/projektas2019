<?php

include_once 'Database.php';

class Image {

  private $images_table = '';

  public function __construct() {
    $this->images_table = 'images';
  }

  public function getImages($id){
    $query = "SELECT 
                    `id`,
                    `image`,
                    `fk_post`
            FROM `{$this->images_table}` 
            WHERE `fk_post`='$id'";

    $data = Database::select($query);

    return $data;
  }

  public function deleteImages($id, $root){
    $images = $this->getImages($id);

    for($i = 0; $i < count($images); $i++) {
      unlink($root . "/public/images/uploads/" . $images[$i]['image']);
    }

    $query = "DELETE FROM `{$this->images_table}` WHERE fk_post='$id'";
    Database::query($query);
  }
}

?>