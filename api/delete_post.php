<?php

  define ('SITE_ROOT', dirname(__DIR__, 1));
  include_once 'config.php';
  include_once 'classes\Post.php';
  include_once 'classes\Image.php';
  
  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];

  $imagesObj = new Image();
  $images = $imagesObj->deleteImages($id, SITE_ROOT);

  $postsObj = new Post();
  $postsObj->deletePost($id);

  echo json_encode('Deleted post');
  
?>