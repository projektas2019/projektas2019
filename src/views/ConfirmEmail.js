import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Cookies from 'universal-cookie';
import { fetchPost } from '../Utils.js';

class ConfirmEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmed: false
    }
  }

  async componentDidMount() {
    const jsonData = {
      email: this.props.match.params.email,
      token: this.props.match.params.token
    };
    await fetchPost('confirm_email.php', jsonData)
      .then((result) => {
        if (result === "Patvirtintas") {
          this.setState({
            confirmed: true
          })
          const cookies = new Cookies();
          cookies.remove('jwt', { path: '/' });
          this.props.userHasLoggedIn(false);
        }
      })

    if (!this.state.confirmed) {
      this.props.history.push('/');
    }
  }

  render() {
    return (
      <div>
        <main>
          {this.state.confirmed
            ? <Typography variant="h5" style={{paddingTop: "20px"}}>Elektroninis paštas patvirtintas. Galite jungtis prie savo paskyros.</Typography>
            : <div></div>}
        </main>
      </div>
    );
  }
}

export default ConfirmEmail;