<?php
  
  include_once 'config.php';
  include_once 'classes\Comment.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];

  $commentsObj = new Comment();
  $data = $commentsObj->listComments($id);

  $comments = array();
  for($i = 0; $i < count($data); $i++) {
    $comments[] = array(
      'id' => $data[$i]['id'],
      'comment' => $data[$i]['comment'],
      'commentPrice' => $data[$i]['price'],
      'created' => $data[$i]['created'],
      'edited' => $data[$i]['edited'],
      'user' => $data[$i]['user']
    );
  }

  echo json_encode($comments);
?>