import React, { Component } from 'react';
import UpdateAccountForm from '../components/UpdateAccountForm';
import { getCities, fetchPost } from '../Utils.js';

class UpdateAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      location: "",
      phone: "",
      cities: [],
      isUpdating: false,
      isLoading: true,
      errorMessage: ""
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.selectElement = this.selectElement.bind(this);
  }

  async componentDidMount() {
    getCities().then((result) => {
      this.setState({ cities: result })
    });

    await fetchPost('get_user.php', { username: this.props.match.params.username })
      .then((result) => {
        this.setState({
          firstName: result.firstName || "",
          lastName: result.lastName || "",
          location: result.location || " ",
          phone: result.phone || ""
        })
      });

    this.setState({ isLoading: false });
  }

  async handleClick(event) {
    this.setState({ isUpdating: true })
    event.preventDefault();

    const jsonArray = Object.assign(this.state, {username: this.props.match.params.username});
    await fetchPost('update_account.php', jsonArray)
      .then((result) => {
        if (result !== "Informacija pakeista") {
          this.setState({
            errorMessage: result
          })
          this.setState({ isUpdating: false });
        }
        else {
          this.setState({
            errorMessage: ""
          })
          this.setState({ isUpdating: false })
          this.props.history.push('/accounts/' + this.props.match.params.username)
        }
      });
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  selectElement() {
    if (this.state.isLoading) {
      return <div></div>
    }
    if (this.props.match.params.username && this.props.match.params.username.length > 0) {
      if (this.props.match.params.username === this.props.currentUser) {
        return <UpdateAccountForm
          handleChange={this.handleChange}
          handleClick={this.handleClick}
          data={this.state}
        />
      }
    }

    return <p className="noUser">Informaciją gali keisti tik vartotojas</p>
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default UpdateAccount;