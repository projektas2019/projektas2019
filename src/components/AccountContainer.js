import React from 'react';
import AccountInfo from './AccountInfo';
import AccountSettings from './AccountSettings';

function AccountContainer(props) {
  return (
    <div style={{ display: "flex", flexDirection: "row" }}>
      <AccountSettings data={props.data} />
      <AccountInfo data={props.data} />
    </div>
  )
}

export default AccountContainer;