import React from 'react';
import Typography from '@material-ui/core/Typography';

const styles = {
  typography: {
    paddingTop: "20px"
  }
}

function AccountInfo(props) {
  return (
    <div>
      <Typography variant="h4" gutterBottom style={{ borderBottom: "3px solid #212121", paddingTop: "15px", display: "inline-block" }}>
        {props.data.username}
      </Typography>

      {props.data.firstName ?
        <Typography variant="body1" style={styles.typography} ><b>Vardas:</b> {props.data.firstName}</Typography> :
        <div></div>}
      {props.data.lastName ?
        <Typography variant="body1" style={styles.typography} ><b>Pavardė:</b> {props.data.lastName}</Typography> :
        <div></div>}
      {props.data.location ?
        <Typography variant="body1" style={styles.typography} ><b>Miestas: </b> {props.data.location}</Typography> :
        <div></div>}
      {props.data.phone ?
        <Typography variant="body1" style={styles.typography} ><b>Telefono nr.:</b> {props.data.phone}</Typography> :
        <div></div>}

      <Typography variant="body1" style={styles.typography} ><b>Registracijos data:</b> {props.data.created}</Typography>
    </div>
  )
}

export default AccountInfo;