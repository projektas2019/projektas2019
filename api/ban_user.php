<?php

  include_once 'config.php';
  include_once 'classes\User.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['username'];

  $userObj = new User();
  $user = $userObj->getUser($username);
  $id = $user['id'];

  $userObj->banUser($id);

  echo json_encode("Vartotojas užblokuotas");
?>

