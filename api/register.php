<?php
  
  include_once 'config.php';
  include_once 'classes\Email.php';
  include_once 'classes\Authentication.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['username'];
  $email = $escaped['email'];
  $password = $escaped['password'];
  $confirmPassword = $escaped['confirmPassword'];

  $message = Authentication::register($username, $email, $password, $confirmPassword);

  if($message !== 'Vartotojas užregistruotas'){
    echo json_encode($message);
    return;
  }

  $usersObj = new User();
  $user = $usersObj->getUser($username);
  $token = $user['token'];

  $emailObj = new Email();
  $subject = "Patvirtinkite savo elektroninį paštą";
  $body = "
            Sveikiname prisijungus prie taisymo skelbimų portalo.<br>
            Paspauskite ant žemiau esančios nuorodos, kad aktyvuotumėte savo paskyrą.<br>
            <a href='http://localhost:3000/confirm/email=$email&token=$token'>Spauskite čia</a>
          ";
  $message = $emailObj->sendEmail($email, $username, $subject, $body);
  
  if($message === 'Email sent'){
    $message = 'User registered';
  }

  echo json_encode($message);

?>