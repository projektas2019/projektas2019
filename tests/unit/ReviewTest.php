<?php

use PHPUnit\Framework\TestCase;
require_once 'api/classes/Review.php';
require_once 'api/classes/Database.php';

class ReviewTest extends TestCase {

  private $db_link;
  private $mysqli;
  private $reviewsObj;

  protected function setUp() :void {
    $this->mysqli = new mysqli("localhost", "root", "", "testdb");
    $sql = file_get_contents('tests/unit/testdb.sql');

    $queries = preg_split('/(;)/', $sql, -1, PREG_SPLIT_DELIM_CAPTURE);
    $queriesWithDelimiters = array();
    for ($i = 0, $n = count($queries) - 1; $i < $n; $i += 2) {
      $queriesWithDelimiters[] = $queries[$i].$queries[$i + 1];
    }
    if ($queries[$n] != '') {
      $queriesWithDelimiters[] = $queries[$n];
    }
    
    foreach ($queriesWithDelimiters as $query) {
      $this->mysqli->query($query) or trigger_error($this->mysqli->error);
    }

    Database::$db = 'testdb';
    $this->reviewsObj = new Review();
  }
  
  public function testCreateReviewSuccess() {
    $message = $this->reviewsObj->createReview(1, 3, "title", "description", 5);

    $this->assertEquals($message, 'Review created');
  }

  public function testCreateReviewFailure() {
    $message = $this->reviewsObj->createReview(1, 1, "title", "description", 5);

    $this->assertEquals($message, 'Įvyko klaida kuriant atsiliepimą');
  }

  public function testCreateReviewNoTitle() {
    $message = $this->reviewsObj->createReview(1, 3, "", "description", 5);

    $this->assertEquals($message, 'Įveskite pavadinimą');
  }
  
  public function testCreateReviewNoDescription() {
    $message = $this->reviewsObj->createReview(1, 3, "title", "", 5);

    $this->assertEquals($message, 'Įveskite atsiliepimą');
  }

  public function testCreateReviewNoRating(){
    $message = $this->reviewsObj->createReview(1, 3, "title", "description", 0);

    $this->assertEquals($message, 'Pasirinkite įvertinimą');
  }

  public function testListReviewsNoReviews(){
    $expected = [];
    $data = $this->reviewsObj->listReviews(3);

    $this->assertEquals($data, $expected);
  }

  public function testListReviewsReviewsExist(){
    $expected = [
      [
        "id" => 11,
        "title" => "f",
        "description" => "sg",
        "created" => "2019-05-04",
        "rating" => 4,
        "user" => "adminas"
      ],
      [
        "id" => 17,
        "title" => "tre",
        "description" => "rtdes",
        "created" => "2019-05-05",
        "rating" => 5,
        "user" => "adminas"
      ]
    ];
    
    $data = $this->reviewsObj->listReviews(6);

    $this->assertEquals($data, $expected);
  }

  public function testUpdateReviewNoTitle(){
    $message = $this->reviewsObj->updateReview(11, "", "naujas description", 2);

    $this->assertEquals($message, "Įveskite pavadinimą");
  }

  public function testUpdateReviewNoDescription(){
    $message = $this->reviewsObj->updateReview(11, "naujas title", "", 2);

    $this->assertEquals($message, "Įveskite atsiliepimą");
  }

  public function testUpdateReviewNoRating(){
    $message = $this->reviewsObj->updateReview(11, "naujas title", "naujas description", 0);

    $this->assertEquals($message, "Pasirinkite įvertinimą");
  }

  public function testUpdateReviewSuccess(){
    $message = $this->reviewsObj->updateReview(11, "naujas title", "naujas description", 2);

    $this->assertEquals($message, "Review updated");
  }

  public function testDeleteReviewSuccess(){
    $success = $this->reviewsObj->deleteReview(11);

    $this->assertEquals($success, true);
  }

  protected function tearDown() :void {
    $this->db_link = mysqli_connect('localhost', 'root', '');
    $sql = 'DROP DATABASE testdb';
    mysqli_query($this->db_link, $sql);
    $sql = 'CREATE DATABASE testdb';
    mysqli_query($this->db_link, $sql);
  }
}

?>