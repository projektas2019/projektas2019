<?php
  
  include_once 'config.php';
  include_once 'classes\Search.php';
  
  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $query = $escaped['query'];
  $createdFrom = $escaped['createdFrom'];
  $createdTo = $escaped['createdTo'];
  $category = $escaped['category'];
  $city = $escaped['city'];

  $searchObj = new Search();
  $data = $searchObj->searchPosts($createdFrom, $createdTo, $category, $city);
  $terms = $searchObj->extractQueryTerms($query);
  $posts = $searchObj->findTermsInData($data, $terms);

  echo json_encode($posts);

?>

