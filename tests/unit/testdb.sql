
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";



CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` char(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `categories` (`id`, `category`) VALUES
(1, 'Kompiuteriai'),
(2, 'Telefonai'),
(3, 'Televizoriai'),
(4, 'Elektronika'),
(5, 'Mikrobangų krosnelės'),
(6, 'Monitoriai'),
(7, 'Virduliai'),
(8, 'Šaldytuvai'),
(9, 'Baldai'),
(10, 'Programinė įranga');

CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Akmenė'),
(2, 'Alytus'),
(3, 'Anykščiai'),
(4, 'Ariogala'),
(5, 'Birštonas'),
(6, 'Biržai'),
(7, 'Druskininkai'),
(8, 'Elektrėnai'),
(9, 'Gargždai'),
(10, 'Ignalina'),
(11, 'Jonava'),
(12, 'Joniškis'),
(13, 'Juodkrantė'),
(14, 'Jurbarkas'),
(15, 'Kaišiadorys'),
(16, 'Kalvarija'),
(17, 'Kaunas'),
(18, 'Kazlų Rūda'),
(19, 'Kelmė'),
(20, 'Kėdainiai'),
(21, 'Klaipėda'),
(22, 'Kretinga'),
(23, 'Kupiškis'),
(24, 'Kuršėnai'),
(25, 'Lazdijai'),
(26, 'Lentvaris'),
(27, 'Marijampolė'),
(28, 'Mažeikiai'),
(29, 'Melnragė'),
(30, 'Molėtai'),
(31, 'Naujoji Akmenė'),
(32, 'Neringa'),
(33, 'Nida'),
(34, 'Pagėgiai'),
(35, 'Pakruojis'),
(36, 'Palanga'),
(37, 'Panevėžys'),
(38, 'Pasvalys'),
(39, 'Plungė'),
(40, 'Prienai'),
(41, 'Radviliškis'),
(42, 'Raseiniai'),
(43, 'Rietavas'),
(44, 'Rokiškis'),
(45, 'Skuodas'),
(46, 'Šakiai'),
(47, 'Šalčininkai'),
(48, 'Šiauliai'),
(49, 'Šilalė'),
(50, 'Šilutė'),
(51, 'Širvintos'),
(52, 'Švenčionys'),
(53, 'Šventoji'),
(54, 'Tauragė'),
(55, 'Telšiai'),
(56, 'Trakai'),
(57, 'Ukmergė'),
(58, 'Utena'),
(59, 'Varėna'),
(60, 'Vievis'),
(61, 'Vilkaviškis'),
(62, 'Vilnius'),
(64, 'Visaginas');


CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `role` varchar(10) CHARACTER SET utf8 NOT NULL,
  `email_token` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creation_date` datetime NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `location_ibfk_3` FOREIGN KEY (`location`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`, `email_token`, `creation_date`, `first_name`, `last_name`, `location`, `phone`) VALUES
(1, 'adminas', '$2y$10$QIR9/bQCV/GUTZT8j6ghb.lP2rSsvt5ZkxCNflLnUqXPFfWkOhMz.', 'pupelislukas053@gmail.com', 'admin', '8e1648b1f08016461a8cd06ee25c5f8585e0cf283f9767b92205fbd98e48487f', '2019-02-10 08:04:03', 'Lukas', 'Pupelis', 11, '+37067586765'),
(18, 'naujas', '$2y$10$nhUic7qlvBDmMFW.vAFbpOibizLK/zLPUEq1twrKMEE0da83DLqW2', 'roe@as.sa', 'unverified', 'eb9c9d8b3b8c27b767a50fc92f5f655df5bdd58de4e44714e3df683d9718dfcf', '2019-04-16 15:44:47', 'Lukas', 'Pupelis', 6, '867586765'),
(21, 'naujasaaa', '$2y$10$H4G1sKFqe4Bh.QFUl1KywOjyYpgmS1yuAvTGeNbdETveCX/t3psTu', 'Naujas@gma.c', 'unverified', '53e08277e3f38edf9548d7a8c37317376c52a1ff6698e2bee851f2744c06747c', '2019-04-28 00:22:19', NULL, NULL, NULL, NULL),
(22, 'Lukas', '$2y$10$MlkCnyv9lzIHMn1JeWMOP.peNbJ87fjWV.R10.DPxdXP5xo0SkzAe', 'asdfjkn@gdas.as', 'unverified', '0fa21f1c498b54c89cb569f495f1c3af06485402a5df136815ec5a60463794be', '2019-05-02 22:12:09', NULL, NULL, NULL, NULL),
(45, 'LukasPupelis1', '$2y$10$XDjLk/idPmqKauvwzj1hBus8yJ/0vh8iPEOpBy2Go6HHA36Fuj4ku', 'pupelis.lukas@gmail.com', 'user', '581504790383aac610bb7d0038c2133249aca73325c89caa068db1045a5059a0', '2019-05-16 01:34:32', NULL, NULL, NULL, NULL);



CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creation_date` datetime NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `location` int(11) NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `price` float(10,2) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fk_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`id`),
  CONSTRAINT `fkc_author` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`),
  CONSTRAINT `location_ibfk_2` FOREIGN KEY (`location`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `posts` (`id`, `title`, `creation_date`, `category`, `description`, `location`, `phone`, `price`, `status`, `fk_user`) VALUES
(196, 'Televizorius', '2019-05-02 01:23:56', 3, 'Sugedo televizorius. Išsijungia kas 30 min.', 30, '867586756', 50.00, 'open', 1),
(197, 'Reikia perrašyti Windows', '2019-05-02 01:24:35', 10, 'Pats nemoku.', 10, '+37069842001', 20.00, 'open', 1),
(198, 'Dulkių siurblys neįsijungia', '2019-05-02 01:26:07', 4, 'Neįsijungia.', 17, '869991042', 100.00, 'open', 1);


CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `price` float(10,2) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `edit_date` datetime DEFAULT NULL,
  `fk_user` int(11) NOT NULL,
  `fk_post` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fkc_commenter` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`),
  CONSTRAINT `fkc_page` FOREIGN KEY (`fk_post`) REFERENCES `posts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `comments` (`id`, `comment`, `price`, `creation_date`, `edit_date`, `fk_user`, `fk_post`) VALUES
(30, 'Laba', NULL, '2019-05-04 19:05:12', NULL, 1, 198),
(31, 'fsdg', 0.00, '2019-05-14 20:37:26', NULL, 1, 198),
(32, 'dsf', 0.00, '2019-05-14 20:40:50', '0000-00-00 00:00:00', 1, 198),
(33, 'fdsgdfs', 0.00, '2019-05-14 20:43:07', NULL, 1, 198),
(34, 'fdsags', 0.00, '2019-05-14 20:43:25', '0000-00-00 00:00:00', 1, 198),
(35, 'rthfy', 0.00, '2019-05-14 20:44:46', NULL, 1, 198),
(36, 'aregd', NULL, '2019-05-14 20:45:06', NULL, 1, 198),
(37, 'ytr', NULL, '2019-05-15 21:57:14', NULL, 1, 198);


CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_post` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fkc_post` FOREIGN KEY (`fk_post`) REFERENCES `posts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `images` (`id`, `image`, `fk_post`) VALUES
(2244, 'g044zcd08mc.jpg', 196),
(2245, 'uq1zu57lbf.png', 198),
(2246, 'c7hswq6o946.jpeg', 198),
(2247, 'wwixnftcuv.jpg', 198);

CREATE TABLE `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creation_date` datetime NOT NULL,
  `category` int(11) NOT NULL,
  `description` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `location` int(11) NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fk_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`category`) REFERENCES `categories` (`id`),
  CONSTRAINT `fkc_repairman` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`location`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `offers` (`id`, `title`, `creation_date`, `category`, `description`, `location`, `phone`, `status`, `fk_user`) VALUES
(3, 'Taisome kompiuterius', '2019-04-17 00:00:00', 1, 'Vilniuje taisome kompiuterius už padorią kainą ir nemokamai prie to pačio perrašinėjame Windows.', 2, '2147483647', 'open', 1),
(4, 'Taisau elektroninius prietaisus', '2019-04-17 00:00:00', 4, 'Priklausomai nuo gedimo tokia ir kaina.', 1, '867577944', 'open', 1),
(5, 'Telefonų taisymas', '2019-04-17 01:29:35', 2, 'Nebrangiai taisome telefonus.', 1, '867994124', 'open', 1),
(6, 'Windows perrašymas nemokamai', '2019-04-17 01:35:31', 10, 'Už ačiū.', 54, '867942100', 'open', 1);

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8 NOT NULL,
  `creation_date` date NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 NOT NULL,
  `rating` smallint(3) NOT NULL,
  `fk_offer` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fkc_offer` FOREIGN KEY (`fk_offer`) REFERENCES `offers` (`id`),
  CONSTRAINT `fkc_reviewer` FOREIGN KEY (`fk_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `reviews` (`id`, `title`, `creation_date`, `description`, `rating`, `fk_offer`, `fk_user`) VALUES
(11, 'f', '2019-05-04', 'sg', 4, 6, 1),
(17, 'tre', '2019-05-05', 'rtdes', 5, 6, 1),
(27, 'p', '2019-05-16', 'p', 5, 5, 45);

ALTER TABLE `cities` AUTO_INCREMENT=65;
ALTER TABLE `users` AUTO_INCREMENT=46;
ALTER TABLE `posts` AUTO_INCREMENT=200;
ALTER TABLE `comments` AUTO_INCREMENT=39;
ALTER TABLE `images` AUTO_INCREMENT=2268;
ALTER TABLE `offers` AUTO_INCREMENT=8;
ALTER TABLE `reviews` AUTO_INCREMENT=33;

COMMIT;