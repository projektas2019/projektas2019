import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

const styles = {
  link: {
    outline: "none",
    textDecoration: "none"
  },
  paper: {
    padding: "5px",
    margin: "20px 0",
    color: "black",
    fontSize: "30px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    height: "200px"
  },
  img: {
    maxHeight: "185px",
    maxWidth: "auto",
    objectFit: "cover"
  }
}

function MiniPost(props) {
  return (
    <Link to={"/posts/" + props.data.id} style={styles.link}>
      <Paper style={styles.paper}>
        <div>
          <Typography variant="h5" style={{paddingBottom: "5px"}}>{props.data.title}</Typography>
          <Typography>Sukūrimo data: {props.data.created}</Typography>
          <Typography>Vartotojas: {props.data.user}</Typography>
          <Typography>Kategorija: {props.data.category}</Typography>
          <Typography>Vietovė: {props.data.location}</Typography>
          {props.data.price > 0 ?
            <Typography>Pageidaujama kaina: {props.data.price} €</Typography> :
            <Typography></Typography>}
        </div>
        <div>
          {props.data.images.length > 0 ?
            <img style={styles.img} src={'/images/uploads/' + props.data.images[0]} alt="" /> :
            <img style={styles.img} src="" alt="" />}
        </div>
      </Paper>
    </Link>
  );
}

export default MiniPost;