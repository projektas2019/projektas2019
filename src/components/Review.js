import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import Link from '@material-ui/core/Link';
import ReviewForm from './ReviewForm';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Radium from 'radium';

const styles = {
  paper: {
    padding: "20px",
    margin: "15px 0",
    border: "1px solid gray"
  },
  editButton: {
    border: "none",
    backgroundColor: "white",
    fontWeight: "bold",
    marginLeft: "20px",
    color: "rgb(14, 106, 148)",
    ':hover': {
      cursor: "pointer",
      textDecoration: "underline"
    }
  },
  deleteButton: {
    border: "none",
    backgroundColor: "white",
    fontWeight: "bold",
    marginLeft: "20px",
    color: "rgb(134, 10, 10)",
    ':hover': {
      cursor: "pointer",
      textDecoration: "underline"
    }
  }
}

function Review(props) {

  function deleteReview() {
    props.deleteReview(props.data.id);
  }

  function editReview() {
    props.editReview(props.data.id);
  }

  return (
    <div>
      {props.reviewEditing === props.data.id ?
        <ReviewForm
          data={{
            user: props.user,
            review: props.data.review,
            reviewTitle: props.data.reviewTitle,
            reviewRating: parseFloat(props.data.reviewRating),
            reviewError: props.reviewError
          }}
          editing={true}
          childProps={props.childProps}
          createReview={props.createReview}
          handleChange={props.handleChange}
          onStarClick={props.onStarClick}
        /> :
        <Paper style={styles.paper}>
          <Typography variant="h6">{props.data.reviewTitle}
            {props.childProps.currentUser !== props.data.user && props.childProps.userRole === "admin" ?
              <div style={{ display: "inline" }}>
                <button key="key3" style={styles.deleteButton} onClick={deleteReview}>Šalinti</button>
              </div> :
              <i></i>}
            {(props.childProps.currentUser === props.data.user && props.childProps.userRole !== "admin") ||
              (props.childProps.userRole === "admin" && props.childProps.currentUser) === props.data.user ?
              <div style={{ display: "inline" }}>
                <button key="key1" style={styles.editButton} onClick={editReview}>Redaguoti</button>
                <button key="key2" style={styles.deleteButton} onClick={deleteReview}>Šalinti</button>
              </div> :
              <i></i>}
          </Typography>
          <Typography style={{ fontStyle: "italic" }}>{props.data.created}</Typography>
          <Link href={"/accounts/" + props.data.user}>{props.data.user}</Link>
          <div className="starRating">
            <StarRatingComponent
              name="rating"
              editing={false}
              starCount={5}
              value={parseFloat(props.data.reviewRating)}
            />
          </div>
          <Typography style={{ padding: "10px 0 0" }}>{props.data.review}</Typography>
        </Paper>}
    </div>
  );
}

export default ((Radium)(Review));