<?php
  
  include_once 'config.php';
  include_once 'classes\User.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['username'];
  $oldPassword = $escaped['oldPassword'];
  $newPassword = $escaped['newPassword'];
  $confirmPassword = $escaped['confirmPassword'];

  $usersObj = new User();
  $message = $usersObj->changePassword($username, $oldPassword, $newPassword, $confirmPassword);
  
  echo json_encode($message);
?>