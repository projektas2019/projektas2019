import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import OfferForm from '../components/OfferForm';
import { getCategories, getCities, fetchPost } from '../Utils.js';

class CreateOffer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      location: "--Pasirinkite miestą--",
      category: "--Pasirinkite kategoriją--",
      phone: "",
      categories: [],
      cities: [],
      errorMessage: "",
      isLoading: true,
      isCreating: false
    };
    this.selectElement = this.selectElement.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  async componentDidMount() {
    await getCategories().then((result) => {
      this.setState({ categories: result })
    });

    await getCities().then((result) => {
      this.setState({ cities: result })
    });

    if (this.props.edit) {
      this.setState({
        title: this.props.data.title,
        description: this.props.data.description,
        category: this.props.data.category,
        location: this.props.data.location,
        phone: this.props.data.phone
      })
    }

    this.setState({ isLoading: false });
  }

  handleClick(event) {
    event.preventDefault();

    this.setState({ isCreating: true });

    let url;
    let message;
    let jsonArray;
    let redirect;

    if (!this.props.edit) {
      jsonArray = Object.assign(this.state, { user: this.props.currentUser });
      message = "Offer created";
      url = "create_offer.php";
      redirect = this.props.history;
    } else {
      jsonArray = Object.assign(this.state, { id: this.props.id });
      message = "Offer updated";
      url = "update_offer.php";
      redirect = this.props.childProps.history
    }

    fetchPost(url, jsonArray)
      .then((result) => {
        if (result !== message) {
          this.setState({
            errorMessage: result,
            isCreating: false
          });
        } else {
          this.setState({
            errorMessage: "",
            isCreating: false
          });
          redirect.push('/offers');
        }
      })
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  selectElement() {
    if (this.state.isLoading) {
      return <p></p>
    }
    if (!this.props.edit) {
      if (!this.props.isLoggedIn) {
        return <Typography variant="h5" style={{paddingTop: "20px"}} >Turite būti prisijungęs, kad galėtumėte kurti naują pasiūlymą.</Typography>
      }
      if (this.props.userRole === 'unverified') {
        return <Typography variant="h5" style={{paddingTop: "20px"}} >Prieš kuriant pasiūlymą, patvirtinkite savo elektroninį paštą.</Typography>
      }
    } else {
      if (this.props.childProps.currentUser !== this.props.data.user) {
        return <Typography variant="h5" style={{paddingTop: "20px"}} >Pasiūlymą redaguoti gali tik jį sukūręs vartotojas.</Typography>
      }
    }

    return <OfferForm
      handleChange={this.handleChange}
      handleClick={this.handleClick}
      data={this.state}
      edit={this.props.edit}
    />
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default CreateOffer;