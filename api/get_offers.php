<?php
  
  include_once 'config.php';
  include_once 'classes\Offer.php';
  
  $offersObj = new Offer();
  $data = $offersObj->listOffers();

  $offers = array();
  for($i = 0; $i < count($data); $i++){
    $offers[] = array(
      'id' => $data[$i]['id'],
      'title' => $data[$i]['title'],
      'created' => $data[$i]['created'],
      'category' => $data[$i]['category'],
      'description' => $data[$i]['description'],
      'location' => $data[$i]['location'],
      'phone' => $data[$i]['phone'],
      'status' => $data[$i]['status'],
      'user' => $data[$i]['user'],
      'rating' => $data[$i]['rating']
    );
  }

  echo json_encode($offers);

?>

