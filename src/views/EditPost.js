import React, { Component } from 'react';
import CreatePost from './CreatePost';
import { fetchPost } from '../Utils';

class EditPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      location: "",
      category: "",
      phone: "",
      price: "",
      images: [],
      isLoading: true
    };
    this.getPostData = this.getPostData.bind(this);
  }

  async componentDidMount() {
    await this.getPostData();
    this.setState({ isLoading: false });
  }

  async getPostData() {
    await fetchPost('get_post.php', { id: this.props.match.params.id })
      .then((result) => {
        this.setState({
          user: result.user,
          title: result.title,
          description: result.description,
          category: result.category,
          location: result.location,
          phone: result.phone,
          price: result.price,
          images: result.images
        });
      })
  }

  selectElement() {
    if (this.state.isLoading) {
      return <main></main>
    }

    return <CreatePost
      childProps={this.props}
      edit={true}
      data={this.state}
      id={this.props.match.params.id}
    />
  }

  render() {
    return (
      <div>
        {this.selectElement()}
      </div>
    );
  }
}

export default EditPost;