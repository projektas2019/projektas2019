import React from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import ErrorIcon from '@material-ui/icons/Error';
import './PostForm.css';

function PostForm(props) {

  function showTempImages() {
    let tmpImages = [];
    for (let i = 0; i < props.data.tempImages.length; ++i) {
      tmpImages[i] =
        <img
          className="uploadedImg"
          key={props.data.imageNames[i]}
          src={props.data.tempImages[i]}
          alt=""
        />;
    }

    return tmpImages;
  }

  return (
    <div>
      <form encType="multipart/form-data" method="post">
        <Typography variant="h4" style={{ paddingTop: "20px" }}>{props.edit ? "Skelbimo redagavimas" : "Naujo skelbimo kūrimas"}</Typography>
        <div>
          <br />
          <TextField
            label="Pavadinimas *"
            margin="normal"
            variant="outlined"
            name="title"
            inputProps={{ maxLength: 64 }}
            style={{ width: "800px" }}
            type="text"
            value={props.data.title}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            label="Aprašymas *"
            margin="normal"
            variant="outlined"
            name="description"
            multiline
            rows="8"
            inputProps={{ maxLength: 2000 }}
            style={{ width: "800px" }}
            type="text"
            value={props.data.description}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            select
            label="Kategorija *"
            name="category"
            value={props.data.category}
            onChange={props.handleChange}
            SelectProps={{
              native: true
            }}
            margin="normal"
            variant="outlined"
          >
            <option>--Pasirinkite kategoriją--</option>
            {props.data.categories.map(category =>
              <option key={category} value={category}>{category}</option>)}
          </TextField>
          <br />
          <TextField
            select
            label="Miestas *"
            name="location"
            value={props.data.location}
            onChange={props.handleChange}
            SelectProps={{
              native: true
            }}
            margin="normal"
            variant="outlined"
          >
            <option>--Pasirinkite miestą--</option>
            {props.data.cities.map(city =>
              <option key={city} value={city}>{city}</option>)}
          </TextField>
          <br />
          <TextField
            label="Telefono numeris *"
            margin="normal"
            variant="outlined"
            name="phone"
            inputProps={{ maxLength: 12 }}
            type="text"
            value={props.data.phone}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            label="Pageidaujama kaina (€)"
            margin="normal"
            variant="outlined"
            name="price"
            inputProps={{ maxLength: 10 }}
            type="text"
            value={props.data.price}
            onChange={props.handleChange}
          />
          <br />
          <input id="file" type="file" name="images[]" multiple onChange={props.uploadPhotos} />
          <label className="uploadPhotos" htmlFor="file">Įkelti nuotraukas</label>
          <div className="selectedFiles">{props.data.imagesMessage}</div>
          <br />
          {showTempImages()}
          <br />
          <Button variant="contained" color="primary" style={{ margin: "20px 0" }} onClick={props.handleClick} disabled={props.data.isCreating}>
            {props.data.isCreating && (
              <i
                className="fa fa-refresh fa-spin"
                style={{ marginRight: "5px" }}
              />
            )}
            {props.edit ? "Išsaugoti" : "Kurti naują skelbimą"}</Button>
          <br />
          {props.data.errorMessage !== "" ?
            <Paper style={{ padding: "10px", backgroundColor: "#F44336", boxShadow: "none", margin: "10px 0", display: "inline-block" }}>
              <ErrorIcon style={{ color: "#FAFAFA", position: "relative", top: "3px" }} /><Typography style={{ color: "#FAFAFA", fontWeight: "bold", display: "inline", paddingLeft: "10px", position: "relative", bottom: "4px" }}>{props.data.errorMessage}</Typography>
            </Paper> :
            <Typography></Typography>}
        </div>
      </form>
    </div>
  )
}

export default PostForm;