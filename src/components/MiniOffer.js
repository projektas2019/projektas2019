import React from 'react';
import Paper from '@material-ui/core/Paper';
import StarRatingComponent from 'react-star-rating-component';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

const styles = {
  link: {
    outline: "none",
    textDecoration: "none"
  },
  paper: {
    padding: "5px",
    display: "block",
    margin: "20px 0",
    color: "black",
    fontSize: "30px",
    height: "200px",
    pointerEvents: "none"
  },
  rating: {
    position: "relative",
    top: "-8px",
    paddingLeft: "10px",
    fontSize: "20px",
    display: "inline"
  }
}

function MiniOffer(props) {
  return (
    <Link to={"/offers/" + props.data.id} style={styles.link} >
      <Paper style={styles.paper} >
        <Typography variant="h5">{props.data.title}</Typography>
        <StarRatingComponent
          name="rating"
          starCount={5}
          value={parseFloat(props.data.rating)}
          renderStarIcon={(index, value) => {
            return <span style={{ color: "rgb(255, 180, 0)" }} className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />;
          }}
          renderStarIconHalf={() => <span style={{ color: "rgb(255, 180, 0)" }} className="fa fa-star-half-full" />}
        />
        <Typography style={styles.rating}>{Math.round(props.data.rating * 100) / 100}</Typography>
        <Typography>Sukūrimo data: {props.data.created}</Typography>
        <Typography>Vartotojas: {props.data.user}</Typography>
        <Typography>Kategorija: {props.data.category}</Typography>
        <Typography>Vietovė: {props.data.location}</Typography>
      </Paper>
    </Link>
  );
}

export default MiniOffer;