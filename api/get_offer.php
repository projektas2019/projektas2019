<?php
  
  include_once 'config.php';
  include_once 'classes\Offer.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $id = $escaped['id'];

  $offersObj = new Offer();
  $offerData = $offersObj->getOffer($id);
    
  $offer = array(
    'title' => $offerData['title'],
    'created' => $offerData['created'],
    'category' => $offerData['category'],
    'description' => $offerData['description'],
    'location' => $offerData['location'],
    'phone' => $offerData['phone'],
    'status' => $offerData['status'],
    'user' => $offerData['user'],
    'rating' => $offerData['rating']
  );

  echo json_encode($offer);

?>

