import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

function Search() {
  return (
    <main>
    <Typography variant="h4" style={{ textAlign: "center", padding: "20px 0 10px" }}>Ieškote skelbimų ar pasiūlymų?</Typography>
      <div style={{display: "flex", justifyContent: "space-around"}}>
        <Button href={"/search/posts"} variant="outlined" style={{padding: "100px 200px", marginTop: "40px", fontSize: "30px"}}>Skelbimai</Button>
        <Button href={"/search/offers"} variant="outlined" style={{padding: "100px 200px", marginTop: "40px", fontSize: "30px"}}>Pasiūlymai</Button>
      </div>
    </main>
  );
}

export default Search;