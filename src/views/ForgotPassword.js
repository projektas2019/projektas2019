import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import ForgotPasswordForm from '../components/ForgotPasswordForm.js';
import { fetchPost } from '../Utils.js';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      errorMessage: "",
      emailSent: false,
      isLoading: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick(event) {
    this.setState({ isLoading: true });
    event.preventDefault();

    await fetchPost('forgot_password.php', this.state)
      .then((result) => {
        if (result !== "Password changed") {
          this.setState({
            errorMessage: result
          });
        } else {
          this.setState({
            errorMessage: "",
            emailSent: true
          });
        }
      });

    this.setState({ isLoading: false });
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  render() {
    return (
      <div>
        {!this.state.emailSent ?
          <ForgotPasswordForm
            handleChange={this.handleChange}
            handleClick={this.handleClick}
            data={this.state}
          /> :
          <Paper style={{margin: "5em auto", width: "600px", height: "auto", padding: "15px", display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            <Typography align="center" variant="h3">Slaptažodis pakeistas</Typography>
            <Typography style={{margin: "10px"}} align="center" variant="body1">
              Į jūsų elektroninį paštą atsiuntėme laišką su nauju slaptažodžiu. 
              Prisijungę, pasikeiskite slaptažodį per vartotojo nustatymus.
            </Typography>
            <Button color="primary" variant="contained" to="/login" component={Link}>Prisijungti</Button>
          </Paper>}
      </div>
    );
  }
}

export default ForgotPassword;