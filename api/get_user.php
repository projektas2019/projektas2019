<?php

  include_once 'config.php';
  include_once 'classes\User.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['username'];

  $userObj = new User();
  $user = $userObj->getUser($username);

  $userData = array(
    'username' => $user['username'],
    'email' => $user['email'],
    'role' => $user['role'],
    'created' => $user['creation_date'],
    'firstName' => $user['first_name'],
    'lastName' => $user['last_name'],
    'location' => $user['city'],
    'phone' => $user['phone']
  );

  echo json_encode($userData);
?>

