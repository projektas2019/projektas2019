import React from 'react';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import ErrorIcon from '@material-ui/icons/Error';

function UpdateAccountForm(props) {
  return (
    <div className="updateAccountContainer">
      <form>
        <Typography variant="h4" style={{ paddingTop: "20px" }}>Informacijos atnaujinimas</Typography>
        <div className="formContainer">
          {props.data.errorMessage !== "" ?
            <Paper style={{ padding: "10px", backgroundColor: "#F44336", boxShadow: "none", margin: "10px 0", display: "inline-block" }}>
              <ErrorIcon style={{ color: "#FAFAFA", position: "relative", top: "3px" }} /><Typography style={{ color: "#FAFAFA", fontWeight: "bold", display: "inline", paddingLeft: "10px", position: "relative", bottom: "4px" }}>{props.data.errorMessage}</Typography>
            </Paper> :
            <Typography></Typography>}
          <br />
          <TextField
            label="Vardas"
            margin="normal"
            variant="outlined"
            name="firstName"
            inputProps={{ maxLength: 30 }}
            type="text"
            value={props.data.firstName}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            label="Pavardė"
            margin="normal"
            variant="outlined"
            name="lastName"
            inputProps={{ maxLength: 30 }}
            type="text"
            value={props.data.lastName}
            onChange={props.handleChange}
          />
          <br />
          <TextField
            select
            label="Miestas"
            name="location"
            value={props.data.location}
            onChange={props.handleChange}
            SelectProps={{
              native: true
            }}
            margin="normal"
            variant="outlined"
          >
            <option>--Pasirinkite miestą--</option>
            {props.data.cities.map(city =>
              <option key={city} value={city}>{city}</option>)}
          </TextField>
          <br />
          <TextField
            label="Telefono nr."
            margin="normal"
            variant="outlined"
            name="phone"
            inputProps={{ maxLength: 12 }}
            type="text"
            value={props.data.phone}
            onChange={props.handleChange}
          />
          <br />
          <Button variant="contained" color="primary" style={{ margin: "20px 0" }} onClick={props.handleClick} disabled={props.data.isUpdating}>
            {props.data.isUpdating && (
              <i
                className="fa fa-refresh fa-spin"
                style={{ marginRight: "5px" }}
              />
            )}
            Atnaujinti informaciją</Button>
        </div>
      </form>
    </div>
  );
}

export default UpdateAccountForm;