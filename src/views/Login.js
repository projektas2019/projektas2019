import React, { Component } from 'react';
import Cookies from 'universal-cookie';
import { fetchPost } from '../Utils.js';
import LoginForm from '../components/LoginForm.js'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      errorMessage: "",
      isLoading: false
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick(event) {
    this.setState({ isLoading: true });
    event.preventDefault();

    await fetchPost('login.php', this.state)
      .then((result) => {
        if (result.indexOf('.') > -1) {
          this.setState({
            errorMessage: ""
          });
          const cookies = new Cookies();
          cookies.set('jwt', result, { path: '/' });
          this.props.childProps.userHasLoggedIn(true);
          this.props.history.push('/');
        } else {
          this.setState({
            errorMessage: result,
            isLoading: false
          });
        }
      });
  };

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  render() {
    return (
      <LoginForm
        handleChange={this.handleChange}
        handleClick={this.handleClick}
        data={this.state}
      />
    );
  }
}

export default Login;