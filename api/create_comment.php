<?php
  
  include_once 'config.php';
  include_once 'classes\User.php';
  include_once 'classes\Comment.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $username = $escaped['username'];
  $post = $escaped['post'];
  $comment = $escaped['comment'];
  $price = $escaped['commentPrice'];

  $usersObj = new User();
  $user = $usersObj->getUser($username);
  
  $commentsObj = new Comment();
  $message = $commentsObj->createComment($user['id'], $post, $comment, $price);

  echo json_encode($message);
?>

