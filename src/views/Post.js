import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import PostContainer from '../components/PostContainer';
import { fetchPost } from '../Utils.js';

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      created: "",
      category: "",
      description: "",
      location: "",
      phone: "",
      price: 0,
      user: "",
      images: [],
      isLoading: true,
      comments: [],
      comment: "",
      commentPrice: "",
      commentError: "",
      commentEditing: 0
    }
    this.selectElement = this.selectElement.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.createComment = this.createComment.bind(this);
    this.editComment = this.editComment.bind(this);
    this.deleteComment = this.deleteComment.bind(this);
    this.listCommments = this.listComments.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  async createComment(event) {
    event.preventDefault();

    if (this.state.commentEditing > 0) {
      for (let i = 0; i < this.state.comments.length; i++) {
        if (this.state.comments[i].id === this.state.commentEditing) {
          await this.setState({
            comment: this.state.comments[i].comment,
            commentPrice: this.state.comments[i].commentPrice > 0 ? this.state.comments[i].commentPrice : ""
          });
        }
      }

      fetchPost('update_comment.php', this.state)
        .then((result) => {
          if (result !== 'Comment updated') {
            this.setState({
              commentError: result
            })
          } else {
            this.setState({
              commentError: "",
              comment: "",
              commentPrice: ""
            })
            this.listComments()
              .then(() => { this.setState({ commentEditing: 0 }) })
          }
        })
    } else {
      const jsonArray = Object.assign(this.state, { username: this.props.currentUser, post: this.props.match.params.id });
      fetchPost('create_comment.php', jsonArray)
        .then((result) => {
          if (result !== 'Comment created') {
            this.setState({
              commentError: result
            })
          } else {
            this.setState({
              commentError: "",
              comment: "",
              commentPrice: ""
            })
            this.listComments();
          }
        })
    }
  }

  deleteComment(commentId) {
    fetchPost('delete_comment.php', { id: commentId })
      .then(() => {
        this.listComments();
      })
  }

  async editComment(commentId) {
    await this.listComments();

    this.setState({
      commentEditing: commentId,
      commentError: ""
    })
  }

  async deletePost() {
    await fetchPost('delete_post.php', {
      id: this.props.match.params.id
    })

    this.props.history.push('/posts');
  }

  async listComments() {
    await fetchPost('list_comments.php', { id: this.props.match.params.id })
      .then((result) => {
        this.setState({
          comments: result
        });
      })
  }

  async componentWillMount() {
    await fetchPost('get_post.php', { id: this.props.match.params.id })
      .then((result) => {
        this.setState({
          title: result.title,
          created: result.created,
          category: result.category,
          description: result.description,
          location: result.location,
          phone: result.phone,
          price: result.price,
          user: result.user,
          images: result.images
        });
      });

    await this.listComments();
    this.setState({ isLoading: false });
  }

  handleChange(event) {
    const { name, value } = event.target;
    if (this.state.commentEditing > 0) {
      for (let i = 0; i < this.state.comments.length; i++) {
        if (this.state.comments[i].id === this.state.commentEditing) {
          const newComments = this.state.comments.slice();
          newComments[i][name] = value;
          this.setState({ comments: newComments });
        }
      }
    } else {
      this.setState({
        [name]: value
      });
    }
  };

  selectElement() {
    if (this.state.isLoading) {
      return <p></p>
    }
    if (this.state.title && this.state.title.length !== "") {
      if (this.props.userRole === "admin" && this.props.currentUser !== this.state.user) {
        return <div className="post">
          <div style={{ display: "flex", justifyContent: "flex-end", float: "right" }}>
            <Button
              variant="contained"
              color="secondary"
              onClick={this.deletePost}
              style={{ margin: "10px" }}
            >
              Ištrinti skelbimą
            </Button>
          </div>
          <PostContainer
            data={this.state}
            childProps={this.props}
            createComment={this.createComment}
            editComment={this.editComment}
            deleteComment={this.deleteComment}
            handleChange={this.handleChange}
          />
        </div>
      }
      return <div className="post">
        {(this.props.currentUser === this.state.user && this.props.userRole !== "admin") ||
          (this.props.userRole === "admin" && this.props.currentUser) === this.state.user ?
          <div style={{ display: "flex", justifyContent: "flex-end", float: "right" }}>
            <Button
              variant="contained"
              color="primary"
              href={"/posts/edit/" + this.props.match.params.id}
              style={{ margin: "10px" }}
            >
              Redaguoti skelbimą
              </Button>
            <Button
              variant="contained"
              color="secondary"
              style={{ margin: "10px" }}
              onClick={this.deletePost}
            >
              Ištrinti skelbimą
              </Button>
          </div> :
          <p></p>}
        <PostContainer
          data={this.state}
          childProps={this.props}
          createComment={this.createComment}
          editComment={this.editComment}
          deleteComment={this.deleteComment}
          handleChange={this.handleChange}
        />
      </div>
    }
    return <Typography variant="h5" style={{ paddingTop: "20px" }}>Skelbimas neegzistuoja.</Typography>
  }

  render() {
    return (
      <main>
        {this.selectElement()}
      </main>
    );
  }
}

export default Post;