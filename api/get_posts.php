<?php

  include_once 'config.php';
  include_once 'classes\Post.php';
  include_once 'classes\Image.php';

  $postsObj = new Post();
  $data = $postsObj->listPosts();

  $imagesObj = new Image();
  $posts = array();

  for($i = 0; $i < count($data); $i++){
    $imagesData = $imagesObj->getImages($data[$i]['id']);
    $images = array();
    for($j = 0; $j < count($imagesData); $j++){
      $images[] = $imagesData[$j]['image'];
    }
    $posts[] = array(
      'id' => $data[$i]['id'],
      'title' => $data[$i]['title'],
      'created' => $data[$i]['created'],
      'category' => $data[$i]['category'],
      'description' => $data[$i]['description'],
      'location' => $data[$i]['location'],
      'phone' => $data[$i]['phone'],
      'price' => $data[$i]['price'],
      'status' => $data[$i]['status'],
      'user' => $data[$i]['user'],
      'images' => $images
    );
  }

  echo json_encode($posts);

?>

