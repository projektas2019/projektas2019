<?php
  
  include_once 'config.php';
  include_once 'classes\Category.php';

  $categoriesObj = new Category();
  $data = $categoriesObj->listCategories();

  $categories = array();
  for($i = 0; $i < count($data); $i++){
    $categories[] = $data[$i]['category'];
  }

  echo json_encode($categories);
  
?>