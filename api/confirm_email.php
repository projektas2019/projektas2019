<?php
  
  include_once 'config.php';
  include_once 'classes\User.php';

  $json = file_get_contents('php://input');
  $obj = json_decode($json, true);
  $escaped = Database::escapeFields($obj);
  $email = $escaped['email'];
  $token = $escaped['token'];

  $usersObj = new User();
  $message = $usersObj->confirmEmail($email, $token);

  echo json_encode($message);
?>