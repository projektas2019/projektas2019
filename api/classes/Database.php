<?php

class Database {

  protected static $connection;
  public static $db = "repair_db";

  public static function connect(){
    if(!isset(self::$connection)){
      $servername = "localhost";
      $db_username = "root";
      $db_password = "";
    
      self::$connection = new mysqli($servername, $db_username, $db_password, Database::$db);
      mysqli_query(self::$connection, "SET NAMES 'utf8'");
      
      if (self::$connection->connect_error) {
        die("Connection failed: " . self::$connection->connect_error);
      }
    }
  
    return self::$connection;
  }

  public static function query($query) {
    $connection = Database::connect();
    $result = $connection->query($query);
    
    return $result;
}

  public static function select($query) {
    $rows = array();
    $result = Database::query($query);
    if($result === false) {
        return false;
    }
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
  }

  public static function error() {
    $connection = Database::connect();
    return $connection->error;
  }

  public static function getLastInsertedId() {
		$connection = Database::connect();
		return mysqli_insert_id($connection);
  }
  
  public static function escapeFields($fields) {
    $connection =  Database::connect();

    $data = array();
		foreach($fields as $key=>$val) {
			$tmp = null;
			if(!is_array($val)) {
				$tmp = mysqli_real_escape_string($connection, $val);
			} else {
        if(count($val) === 0){
          $tmp = array();
        } else {
          foreach($val as $key2 => $val2) {
            if(!is_array($val2)){
              $tmp[] = mysqli_real_escape_string($connection, $val2);
            } else {
              if(count($val2) === 0){
                $tmp[$key2] = array();
              } else {
                foreach($val2 as $key3 => $val3){
                  $tmp[$key2][] = mysqli_real_escape_string($connection, $val3);
                }
              }
            }
          }
        }
      }

			$data[$key] = $tmp;	
		}

		return $data;
	}
}

?>