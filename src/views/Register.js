import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import RegisterForm from '../components/RegisterForm.js';
import { fetchPost } from '../Utils.js';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      confirmPassword: "",
      email: "",
      errorMessage: "",
      registered: false,
      isLoading: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  async handleClick(event) {
    this.setState({ isLoading: true });
    event.preventDefault();

    await fetchPost('register.php', this.state)
      .then((result) => {
        if (result !== "User registered") {
          this.setState({
            errorMessage: result
          });
        } else {
          this.setState({
            errorMessage: ""
          });
          this.setState({ registered: true })
        }
      });

    this.setState({ isLoading: false });
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  render() {
    return (
      <div>
        {!this.state.registered ?
          <RegisterForm
            handleChange={this.handleChange}
            handleClick={this.handleClick}
            data={this.state}
          /> :
          <Paper style={{margin: "5em auto", width: "600px", height: "auto", padding: "15px", display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            <Typography align="center" variant="h3">Vartotojas užregistruotas</Typography>
            <Typography style={{margin: "10px"}} align="center" variant="body1">Patvirtinkite savo elektroninį paštą, paspausdami elektroniniu laišku gautą nuorodą ir prisijunkite.</Typography>
            <Button color="primary" variant="contained" to="/login" component={Link}>Prisijungti</Button>
          </Paper>}
      </div>
    );
  }
}

export default Register;