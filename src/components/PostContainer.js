import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import Comment from './Comment';
import CommentForm from './CommentForm';

const styles = {
  image: {
    padding: "2px",
    margin: "10px 10px 10px 0",
    border: "1px solid gray",
    height: "300px",
    width: "300px",
    objectFit: "contain",
    borderRadius: "10px"
  },
  paper: {
    padding: "20px",
    margin: "20px 0 10px",
    fontSize: "30px"
  }
}

function PostContainer(props) {
  const images = props.data.images.map(image =>
    <img
      style={styles.image}
      key={image.id}
      src={'/images/uploads/' + image.image}
      alt=""
    />);

  const comments = props.data.comments.map(comment =>
    <Comment
      key={comment.id}
      data={comment}
      user={props.data.user}
      commentEditing={props.data.commentEditing}
      commentError={props.data.commentError}
      childProps={props.childProps}
      createComment={props.createComment}
      editComment={props.editComment}
      deleteComment={props.deleteComment}
      handleChange={props.handleChange}
    />);

  return (
    <div>
      <Paper style={styles.paper}>
        <Typography variant="h4">{props.data.title}</Typography>
        <Typography style={{padding: "20px 0"}}>{props.data.description}</Typography>
        <Typography><b>Sukūrimo data:</b> {props.data.created}</Typography>
        <Typography><b>Vartotojas:</b> <Link href={"/accounts/" + props.data.user} >{props.data.user}</Link></Typography>
        <Typography><b>Kategorija:</b> {props.data.category}</Typography>
        <Typography><b>Vietovė:</b> {props.data.location}</Typography>
        <Typography><b>Telefono numeris:</b> {props.data.phone}</Typography>

        {props.data.price > 0 ?
          <Typography><b>Pageidaujama kaina:</b> {props.data.price} €</Typography> :
          <Typography></Typography>}

        {props.data.images.length > 0 ?
          <div>
            <Typography><b>Pridėti paveikslėliai:</b></Typography>
            {images}
          </div> :
          <Typography></Typography>}
      </Paper>
      <div className="commentsContainer">
        <Typography variant="h5" style={{ paddingTop: "20px" }}>Komentarai</Typography>
        <div>
          {comments}
        </div>
        {props.childProps.isLoggedIn && props.childProps.userRole !== 'unverified' && props.data.commentEditing === 0 ?
          <CommentForm
            data={props.data}
            childProps={props.childProps}
            createComment={props.createComment}
            handleChange={props.handleChange}
          /> :
          <p className="spacing">&nbsp;</p>}
      </div>
    </div>
  )
}

export default PostContainer;