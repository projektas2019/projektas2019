import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import StarRatingComponent from 'react-star-rating-component';
import Review from './Review';
import ReviewForm from './ReviewForm';

const styles = {
  paper: {
    padding: "20px",
    margin: "20px 0 10px",
    fontSize: "30px"
  },
  rating: {
    paddingLeft: "10px",
    fontSize: "20px",
    display: "inline",
    position: "relative",
    bottom: "8px"
  }
}

function OfferContainer(props) {
  function hasUserMadeReview() {
    for (let i = 0; i < props.data.reviews.length; i++) {
      if (props.data.reviews[i].user === props.childProps.currentUser) {
        return true;
      }
    }
    return false;
  }

  const reviews = props.data.reviews.map(review =>
    <Review
      key={review.id}
      data={review}
      user={props.data.user}
      reviewEditing={props.data.reviewEditing}
      reviewError={props.data.reviewError}
      childProps={props.childProps}
      createReview={props.createReview}
      editReview={props.editReview}
      deleteReview={props.deleteReview}
      handleChange={props.handleChange}
      onStarClick={props.onStarClick}
    />);

  return (
    <div>
      <Paper style={styles.paper}>
        <Typography variant="h4" style={{paddingBottom: "10px"}}>{props.data.title}</Typography>
        <StarRatingComponent
          name="rating"
          editing={false}
          starCount={5}
          value={props.data.rating}
          renderStarIcon={(index, value) => {
            return <span style={{ color: "rgb(255, 180, 0)" }} className={index <= value ? 'fa fa-star' : 'fa fa-star-o'} />;
          }}
          renderStarIconHalf={() => <span style={{ color: "rgb(255, 180, 0)" }} className="fa fa-star-half-full" />}
        />
        <Typography style={styles.rating}>{props.data.rating}</Typography>
        <Typography style={{ padding: "20px 0" }}>{props.data.description}</Typography>
        <Typography><b>Sukūrimo data:</b> {props.data.created}</Typography>
        <Typography><b>Vartotojas:</b> <Link href={"/accounts/" + props.data.user}>{props.data.user}</Link></Typography>
        <Typography><b>Kategorija:</b> {props.data.category}</Typography>
        <Typography><b>Vietovė:</b> {props.data.location}</Typography>
        <Typography><b>Telefono numeris:</b> {props.data.phone}</Typography>
      </Paper>
      <div className="reviewsContainer">
        <Typography variant="h5" style={{ paddingTop: "20px" }}>Atsiliepimai</Typography>
        <div>
          {reviews}
        </div>
        {props.childProps.isLoggedIn &&
          props.childProps.userRole !== 'unverified' &&
          props.data.reviewEditing === 0 &&
          props.childProps.currentUser !== props.data.user &&
          !hasUserMadeReview() ?
          <ReviewForm
            data={props.data}
            childProps={props.childProps}
            createReview={props.createReview}
            handleChange={props.handleChange}
            onStarClick={props.onStarClick}
          /> :
          <p className="spacing">&nbsp;</p>}
      </div>
    </div>
  )
}

export default OfferContainer;