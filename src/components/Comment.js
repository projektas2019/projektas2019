import React from 'react';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Radium from 'radium';
import CommentForm from './CommentForm';

const styles = {
  paper: {
    padding: "20px",
    margin: "15px 0",
    border: "1px solid gray"
  },
  editButton: {
    border: "none",
    backgroundColor: "white",
    fontWeight: "bold",
    marginLeft: "20px",
    color: "rgb(14, 106, 148)",
    ':hover': {
      cursor: "pointer",
      textDecoration: "underline"
    }
  },
  deleteButton: {
    border: "none",
    backgroundColor: "white",
    fontWeight: "bold",
    marginLeft: "20px",
    color: "rgb(134, 10, 10)",
    ':hover': {
      cursor: "pointer",
      textDecoration: "underline"
    }
  }
}

function Comment(props) {

  function deleteComment() {
    props.deleteComment(props.data.id);
  }

  function editComment() {
    props.editComment(props.data.id);
  }

  return (
    <div>
      {props.commentEditing === props.data.id ?
        <CommentForm
          data={{
            user: props.user,
            comment: props.data.comment,
            commentPrice: props.data.commentPrice > 0 ? props.data.commentPrice : "",
            commentError: props.commentError
          }}
          editing={true}
          childProps={props.childProps}
          createComment={props.createComment}
          handleChange={props.handleChange}
        /> :
        <Paper style={styles.paper}>
          <Link href={"/accounts/" + props.data.user}>{props.data.user}</Link>
          {props.childProps.currentUser !== props.data.user && props.childProps.userRole === "admin" ?
            <div style={{ display: "inline" }}>
              <button key="key1" style={styles.deleteButton} onClick={deleteComment}>Šalinti</button>
            </div> :
            <i></i>
          }
          {(props.childProps.currentUser === props.data.user && props.childProps.userRole !== "admin") ||
            (props.childProps.userRole === "admin" && props.childProps.currentUser) === props.data.user ?
            <div style={{ display: "inline" }}>
              <button key="key2" style={styles.editButton} onClick={editComment}>Redaguoti</button>
              <button key="key3" style={styles.deleteButton} onClick={deleteComment}>Šalinti</button>
            </div> :
            <i></i>
          }
          <Typography style={{ fontStyle: "italic" }}>{props.data.created}</Typography>
          <Typography style={{ padding: "10px 0" }}>{props.data.comment}</Typography>
          {props.data.commentPrice > 0 ?
            <Typography><b>Siūloma kaina:</b> {props.data.commentPrice}</Typography> :
            <Typography></Typography>}
        </Paper>}
    </div>
  );
}

export default ((Radium)(Comment));