<?php

include_once 'Database.php';
include_once 'Category.php';
include_once 'City.php';

class Post {

  private $posts_table = '';
  private $users_table = '';
  private $cities_table = '';
  private $categories_table = '';
  private $images_table = '';
  private $comments_table = '';

  public function __construct() {
    $this->posts_table = 'posts';
    $this->users_table = 'users';
    $this->cities_table = 'cities';
    $this->categories_table = 'categories';
    $this->images_table = 'images';
    $this->comments_table = 'comments';
  }

  public function createPost($user, $title, $description, $category, $city, $phone, $price, $images){
    if($title === ""){
      return "Įveskite skelbimo pavadinimą";
    }
    if($description === ""){
      return "Įveskite skelbimo aprašymą";
    }
    if($phone === ""){
      return "Įveskite savo telefono numerį";
    }
    if(!(preg_match('/^[+370][0-9]*$/', $phone) && strlen($phone) === 12) &&
       !(preg_match('/^[8][0-9]*$/', $phone) && strlen($phone) === 9)){
      return "Telefono numerio formatas turi būti +370******** arba 8********";
    }
    if($price === ""){
      $price = NULL;
    } else if (!is_numeric($price) || $price <= 0){
      return "Netinkama kaina"; 
    } else {
      number_format($price, 2, '.', '');
    }
  
    date_default_timezone_set('Europe/Riga');
    $creation_date = date('Y-m-d H:i:s');
  
    $query = "INSERT INTO `{$this->posts_table}`
              (
                `title`, 
                `creation_date`,
                `category`, 
                `description`, 
                `location`, 
                `phone`, 
                `price`, 
                `status`, 
                `fk_user`
              ) 
              VALUES 
              (
                '$title', 
                '$creation_date', 
                '$category', 
                '$description', 
                '$city', 
                '$phone', 
                NULLIF('$price', 0.00), 
                'open', 
                '$user'
              )";
    
    Database::query($query);
  
    $this->insertImages(Database::getLastInsertedId(), $images);

    return 'Post created';
  }

  public function getPost($id){
    $query = "SELECT 
                    `{$this->posts_table}`.`id`, 
                    `{$this->posts_table}`.`title`,
                    `{$this->posts_table}`.`creation_date` AS `created`,
                    `{$this->posts_table}`.`category`,
                    `{$this->posts_table}`.`description`,
                    `{$this->posts_table}`.`location`,
                    `{$this->posts_table}`.`phone`,
                    `{$this->posts_table}`.`price`,
                    `{$this->posts_table}`.`status`,
                    `{$this->users_table}`.`username` AS `user`,
                    `{$this->categories_table}`.`category` AS `category`,
                    `{$this->cities_table}`.`name` AS `location`
              FROM `{$this->posts_table}`
              INNER JOIN `{$this->users_table}` ON `{$this->posts_table}`.`fk_user`=`{$this->users_table}`.`id`
              INNER JOIN `{$this->categories_table}` ON `{$this->posts_table}`.`category`=`{$this->categories_table}`.`id`
              INNER JOIN `{$this->cities_table}` ON `{$this->posts_table}`.`location`=`{$this->cities_table}`.`id`
              WHERE `{$this->posts_table}`.`id`='$id'";

    $data = Database::select($query);

    if(!$data){
      return false;
    }

    return $data[0];
  }

  public function listPosts(){
    $query = "SELECT 
                    `{$this->posts_table}`.`id`, 
                    `{$this->posts_table}`.`title`,
                    `{$this->posts_table}`.`creation_date` AS `created`,
                    `{$this->posts_table}`.`category`,
                    `{$this->posts_table}`.`description`,
                    `{$this->posts_table}`.`location`,
                    `{$this->posts_table}`.`phone`,
                    `{$this->posts_table}`.`price`,
                    `{$this->posts_table}`.`status`,
                    `{$this->users_table}`.`username` AS `user`,
                    `{$this->categories_table}`.`category` AS `category`,
                    `{$this->cities_table}`.`name` AS `location`
              FROM `{$this->posts_table}`
              INNER JOIN `{$this->users_table}` ON `{$this->posts_table}`.`fk_user`=`{$this->users_table}`.`id`
              INNER JOIN `{$this->categories_table}` ON `{$this->posts_table}`.`category`=`{$this->categories_table}`.`id`
              INNER JOIN `{$this->cities_table}` ON `{$this->posts_table}`.`location`=`{$this->cities_table}`.`id`
              ORDER BY `{$this->posts_table}`.`creation_date` DESC";

    $data = Database::select($query);

    return $data;
  }

  public function updatePost($id, $title, $description, $category, $location, $phone, $price){
    if($title === ""){
      return "Įveskite skelbimo pavadinimą";
    }
    if($description === ""){
      return "Įveskite skelbimo aprašymą";
    }
    if($category === "" || $category === "--Pasirinkite kategoriją--"){
      return "Pasirinkite kategoriją";
    }
    if($location === "" || $location === "--Pasirinkite miestą--"){
      return "Pasirinkite miestą";
    }
    if($phone === ""){
      return "Įveskite savo telefono numerį";
    }
    if(!(preg_match('/^[+370][0-9]*$/', $phone) && strlen($phone) === 12) &&
       !(preg_match('/^[8][0-9]*$/', $phone) && strlen($phone) === 9)){
      return "Telefono numerio formatas turi būti +370******** arba 8********";
    }
    if(!is_float($price) && $price < 0 && $price !== ""){
      return "Įvesta kaina netinkama";
    }
    if($price === ""){
      $price = NULL;
    } else if (!is_numeric($price) || $price <= 0){
      return "Netinkama kaina"; 
    } else {
      number_format($price, 2, '.', '');
    }

    $categoriesObj = new Category();
    $data = $categoriesObj->getCategory($category);
    $category = $data['id'];

    $citiesObj = new City();
    $data = $citiesObj->getCity($location);
    $location = $data['id'];

    $query = "UPDATE `{$this->posts_table}` 
              SET 
                  `title`='$title', 
                  `description`='$description', 
                  `category`='$category', 
                  `phone`='$phone', 
                  `location`='$location', 
                  `price`=NULLIF('$price', 0.00) 
              WHERE `id`='$id'";
    
    Database::query($query);

    return 'Post updated';
  }

  public function deletePost($id){
    $query = "DELETE FROM `{$this->comments_table}` WHERE fk_post='$id'";
    Database::query($query);

    $query = "DELETE FROM `{$this->posts_table}` WHERE id='$id'";
    Database::query($query);
  }

  public function insertImages($post_id, $images){
    for($i = 0; $i < count($images); $i++){
      $name = $images[$i];
      $query = "INSERT INTO `{$this->images_table}` 
                (
                  image, 
                  fk_post
                ) 
                VALUES 
                (
                  '$name', 
                  '$post_id'
                )";
      Database::query($query);
    }
  }
}


?>